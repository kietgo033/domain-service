package net.lvs.domain.sdk.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DomainSDKException extends Exception {
    protected String message;
}
