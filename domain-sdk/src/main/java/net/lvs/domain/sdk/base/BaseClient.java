package net.lvs.domain.sdk.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import lombok.Getter;
import lombok.SneakyThrows;
import net.lvs.domain.sdk.exceptions.DomainSDKException;
import net.lvs.domain.utils.Constants;
import net.lvs.domain.wrapper.ResponseWrapper;
import org.apache.commons.validator.GenericValidator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class BaseClient {
    private final Gson gson = new GsonBuilder()
            .registerTypeAdapter(Date.class, new DateTypeAdapter())
            .create();
    protected final String separator = "/";

    @Getter
    protected String urlService;

    @Getter
    protected String token;


    //FUNCTION==========================================================================================================
    @SneakyThrows
    protected <T> T doGet(String path, Object data, Type type) {
        return doRequest(path, "GET", data, type);
    }

    @SneakyThrows
    protected <T> T doPost(String path, Object data, Type type) {
        return doRequest(path, "POST", data, type);
    }

    @SneakyThrows
    protected <T> T doPut(String path, Object data, Type type) {
        return doRequest(path, "PUT", data, type);
    }

    @SneakyThrows
    protected <T> T doDelete(String path, Object data, Type type) {
        return doRequest(path, "DELETE", data, type);
    }

    @SneakyThrows
    protected <T> T doHead(String path, Object data, Type type) {
        return doRequest(path, "HEAD", data, type);
    }
    //OTHER=============================================================================================================

    @SneakyThrows
    protected <T> T doRequest(String path, String method, Object data, Type type) {
        HttpURLConnection connection = getConnection(path, method);
        if (null != data) {
            OutputStream os = connection.getOutputStream();
            String json = gson.toJson(data);
            byte[] dataBytes = json.getBytes();
            os.write(dataBytes);
            os.flush();
            os.close();
        }
        String result = getJsonResult(connection);
        return gson.fromJson(result, type);
    }

    @SneakyThrows
    private HttpURLConnection getConnection(String path, String method) {
        if (GenericValidator.isBlankOrNull(Constants.URL_SERVICE)) {
            throw new Exception("Not found service URL");
        }
        URL url = new URL(urlService + Constants.URL_SERVICE + separator + path);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestMethod(method);
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        if (!GenericValidator.isBlankOrNull(token)) {
            httpURLConnection.setRequestProperty(Constants.HEADER_TOKEN_API, token);
        }
        return httpURLConnection;
    }

    @SneakyThrows
    private String getJsonResult(HttpURLConnection conn) {
        String result = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } catch (Exception e) {
            br = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        } finally {
            if (br != null) {
                StringBuilder builder = new StringBuilder();
                String output;
                while ((output = br.readLine()) != null) {
                    builder.append(output);
                }
                br.close();
                result = builder.toString().trim();
            }
        }
        return result;
    }

    protected <T> T getData(ResponseWrapper<T> wrapper) throws DomainSDKException {
        if (wrapper.getStatus() == 1) {
            return wrapper.getData();
        } else {
            throw new DomainSDKException(wrapper.getMessage());
        }
    }

}
