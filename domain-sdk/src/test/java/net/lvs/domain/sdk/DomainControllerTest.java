package net.lvs.domain.sdk;

import net.lvs.domain.dto.CreateDomainDTO;
import net.lvs.domain.dto.inet.*;
import net.lvs.domain.model.domain.*;
import net.lvs.domain.sdk.exceptions.DomainSDKException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 09:11 01/03/2022
 * Admin
 */

public class DomainControllerTest {
    public static DomainController mACC;

    @BeforeAll
    public static void before() {
        mACC = new DomainController("http://localhost:8081/domain-service/", "112265");
    }


    @Test
    public void testSearchDomainInet() throws DomainSDKException {
        boolean rs=false;
        try {
            DomainINetModel domainINetModel = new DomainINetModel();
            domainINetModel.setVerifyStatus("not-verdfsify");
            List<ResultInfoDomainINetModel> list=mACC.searchInfoDomainInet(domainINetModel);
            System.out.println(list.size());
            System.out.println(list.get(0).getId());
            rs=true;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        Assertions.assertEquals(true, rs);
    }



    //////////////////test thử /////////
//    @Test
//    public void testCreateDomainInet() throws DomainSDKException {
//        boolean rs=false;
//        try {
//            CreateDomainDTO createDomainDTO = new CreateDomainDTO();
//            InetCreateDomainDTO inetCreateDomainDTO=new InetCreateDomainDTO();
//            inetCreateDomainDTO.setName("phan.ngusiii");
//            inetCreateDomainDTO.setPeriod(1);
//
//            createDomainDTO.setInetCreateDomainDTO(inetCreateDomainDTO);
//            mACC.createDomain(createDomainDTO,"inet");
//            rs=true;
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
//        Assertions.assertEquals(false, rs);
//    }
//





    // Có thể check cho cả Pa hoặc Inet ở đầu vào configKey
    @Test
    public void testCheckExistDomain() throws DomainSDKException {
        String domain="test.vn";
        String configKey="PA-VietNam";
        Assertions.assertEquals(true, mACC.checkExistDomain(domain,configKey));
    }

    @Test
    public void getInfoDomain() throws DomainSDKException {
        String id = "2753711";
        String configKey="inet";
        boolean rs=false;
        ResultDomainModel resultDomainModel=null;
        try {
            resultDomainModel=mACC.getInfoDomain(id,configKey);
            rs =true;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println(resultDomainModel.getExpireDate());

        Assertions.assertEquals(true, rs);
    }

    @Test
    //La domain quoc te doi voi Pa hoac la dung de cap nhat dns ben Inet
    public void updateDNSDomain() throws DomainSDKException {
        String configKey="Pa";

        InetUpdateDNSDomainDTO inetUpdateDNSDomainDTO = new InetUpdateDNSDomainDTO();
        inetUpdateDNSDomainDTO.setId("564312462");
        HostNameINetModel hostNameModel1 = new HostNameINetModel("ns101.inett.vn");
        HostNameINetModel hostNameModel2 = new HostNameINetModel("ns102.inett.vn");
        HostNameINetModel hostNameModel3 = new HostNameINetModel("ns103.inett.vn");
       // HostNameINetModel hostNameModel4 = new HostNameINetModel("ns104.inett.vn");


        List<HostNameINetModel> list = new ArrayList<>();
        list.add(hostNameModel1);
        list.add(hostNameModel2);
        list.add(hostNameModel3);
        //list.add(hostNameModel4);
        inetUpdateDNSDomainDTO.setNsList(list);
        boolean rs=false;
        try {
            rs=mACC.updateDNSDomain(inetUpdateDNSDomainDTO,configKey);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            rs=false;
        }
        Assertions.assertEquals(false, rs);
    }


     @Test
    //La domain quoc te doi voi Pa hoac la dung de cap nhat dns ben Inet
    public void updateDNSDomainChild() throws DomainSDKException {
        String configKey="INet-VietNam";

       InetUpdateDNSChildDTO inetUpdateDNSChildDTO=new InetUpdateDNSChildDTO();
       inetUpdateDNSChildDTO.setId("5642151311");

       List<HostNameModel> list= new ArrayList<>();

       HostNameModel h1=new HostNameModel("host1","ip1");
       HostNameModel h2=new HostNameModel("host2","ip2");
       HostNameModel h3=new HostNameModel("host3","ip3");

       list.add(h1);
       list.add(h2);
       list.add(h3);

       inetUpdateDNSChildDTO.setHostList(list);

        boolean rs=false;
        try {
            rs=mACC.updateDNSDomainChild(inetUpdateDNSChildDTO,configKey);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            rs=false;
        }
        Assertions.assertEquals(false, rs);
    }






//    @Test
//    //La domain quoc te doi voi Pa hoac la dung de cap nhat dns ben Inet
//    public void updateDNSDomainChild() throws DomainSDKException {
//        //String configKey="PA-VietNam";
//        String configKey="INet-VietNam";
//
//        InetUpdateDNSChildDTO inetUpdateDNSChildDTO = new InetUpdateDNSChildDTO();
//        inetUpdateDNSChildDTO.setId("5356356354");
////        HostNameINetModel hostNameINetModel1 = new HostNameINetModel("ns101.inett.vn","192.168.1.1");
////        HostNameINetModel hostNameINetModel2 = new HostNameINetModel("ns112.inett.vn","192.168.1.2");
////        HostNameINetModel hostNameINetModel3 = new HostNameINetModel("ns113.inett.vn","192.168.1.3");
////        HostNameINetModel hostNameINetModel4 = new HostNameINetModel("ns114.inett.vn","192.168.1.4");
//
//        List<HostNameINetModel> list = new ArrayList<>();
////        list.add(hostNameINetModel1);
////        list.add(hostNameINetModel2);
////        list.add(hostNameINetModel3);
////        list.add(hostNameINetModel4);
//        inetUpdateDNSChildDTO.setHostList(list);
//        boolean rs=false;
//        try {
//            rs=mACC.updateDNSChildDomain(inetUpdateDNSChildDTO,configKey);
//        }catch (Exception e)
//        {
//            System.out.println(e.getMessage());
//            rs=false;
//        }
//        Assertions.assertEquals(false, rs);
//    }

    @Test
    public void privacyProtectionDomainInet() throws DomainSDKException {
        String id = "2086072";
        Assertions.assertEquals(false, mACC.privacyProtectionDomainInet(id));
    }

    @Test
    public void checkWhoIsDomainInet() throws DomainSDKException {
        String domainName = "itestonline.org";
        System.out.println(mACC.checkWhoIsDomainInet(domainName));

    }

    @Test
    public void createAccountInet() {
        ContactINetModel contactINetModel = new ContactINetModel();
        contactINetModel.setEmail("tpkietvt@gmail.com");
        contactINetModel.setAddress("Tien Giang");
        contactINetModel.setPassword("PhanHoangKiet");
        contactINetModel.setFullname("PhanHoangKiet");
        contactINetModel.setOrganizationName("Long Van");
        contactINetModel.setBirthday("1999-10-17 00:00");
        contactINetModel.setPhone("0989588925");

        boolean expected = false;
        boolean rs;
        try {
            ContactINetModel rsContact = mACC.createAccountCustomerDomainInet(contactINetModel);
            rs = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);
    }


    @Test
    public void suspendAccountInet() {
        String id = "409808424";
        boolean expected = false;
        boolean rs;
        try {
            rs = mACC.suspendAccountCustomerDomainInet(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);

    }

    @Test
    public void activeAccountInet() {
        String id = "409808";
        boolean expected = true;
        boolean rs;
        try {
            rs = mACC.activeAccountCustomerDomainInet(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);

    }

    @Test
    public void getAccountInetByEmail() {
        String email = "zloldfk@gmail.com";
        boolean expected = true;
        boolean rs;
        try {
            mACC.getAccountCustomerDomainInetByEmail(email);
            rs = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);

    }

    @Test
    public void getAccountInetByid() {
        String id = "409808";
        boolean expected = true;
        boolean rs;
        try {
            mACC.getAccountCustomerDomainInetById(id);
            rs = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);

    }

    @Test
    public void getAccountStillMoney() {
        boolean expected = true;
        boolean rs;
        try {
            System.out.println("So tien con lai la: " + mACC.getAccountStill().getMoney() + " VND");
            rs = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);
    }

    @Test
    public void getAccountTotalMoney() {
        boolean expected = true;
        boolean rs;
        try {
            System.out.println("Tong so tien la: " + mACC.getAccountTotal().getMoney() + " VND");
            rs = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);
    }

    @Test
    public void getDatePaDomain() {
        boolean expected =false;
        boolean rs;
        try {
            System.out.println("Date: " + mACC.getDateDomainPa("test.vn").getDate());
            rs = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            rs = false;
        }
        Assertions.assertEquals(expected, rs);
    }
    @Test
    public void checkDomainPa() {
        int rs=10;
        try {
            String domain="phanngusiii.com";
            rs=mACC.checkDomainPa(domain);
            System.out.println("Code: " + rs);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Assertions.assertEquals(0, rs);
    }

    @Test
    public void testChangePasswordDomain() throws DomainSDKException {
        InetChangePasswordDTO inetChangePasswordDTO=new InetChangePasswordDTO();
        String id="phanhoangngusai.com";
        String password="123456Abc";

        inetChangePasswordDTO.setId(id);
        inetChangePasswordDTO.setPassword(password);
        String configKey="PA-VietNam";
        boolean rs=false;
        try {
            rs=mACC.changePassword(inetChangePasswordDTO,configKey);
        }catch (Exception e){
            System.out.println(e.getMessage());
            rs=false;
        }
        Assertions.assertEquals(false,rs);
    }






    ///////////////-------Inet-----/////////////////
    @Test
    public void testGetRecordDomainInet() throws DomainSDKException {

        String id="2349003";
        boolean re=false;
        ResultDomainModel rs;
        try {
            rs=mACC.getRecordDomainInet(id);
            re=true;
        }catch (Exception e){
            System.out.println(e.getMessage());
            re=false;
        }
        Assertions.assertEquals(true,re);
    }




}
