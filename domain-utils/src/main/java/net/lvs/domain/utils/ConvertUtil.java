package net.lvs.domain.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class ConvertUtil {
    public final static String MAIL_SERVER_KEY = "VGRSrtNhrvI=";
    public static String convertKB(double value) {
        if (value > 0) {
            if (value < 1024) {
                return value + " KB";
            }
            if (value < (1024 * 1024)) {
                double val = value / 1024;
                val = (double) Math.round(val * 1000) / 1000;
                return val + " MB";
            }
            if (value < (1024 * 1024 * 1024)) {
                double val = value / (1024 * 1024);
                val = (double) Math.round(val * 1000) / 1000;
                return val + " GB";
            }
            double val = value / (1024 * 1024 * 1024);
            val = (double) Math.round(val * 1000) / 1000;
            return val + " TB";
        }
        return "0";
    }
    public static String encode(String password) {
        try {
            return encrypt(password, MAIL_SERVER_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String decode(String password) {
        try {
            return decrypt(password, MAIL_SERVER_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public static Key generateKey() throws NoSuchAlgorithmException {
        KeyGenerator generator;
        generator = KeyGenerator.getInstance("DES");
        generator.init(new SecureRandom());
        return generator.generateKey();
    }


    private static String encrypt(String message, Key key)
            throws IllegalBlockSizeException, BadPaddingException,
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] stringBytes = message.getBytes("UTF8");

        byte[] raw = cipher.doFinal(stringBytes);

        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(raw);
    }


    private static String encrypt(String message, String strKey)
            throws IllegalBlockSizeException, BadPaddingException,
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] encodedKey = decoder.decodeBuffer(strKey);
        Key key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "DES");
        return encrypt(message, key);
    }

    private static String decrypt(String encrypted, Key key) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException,
            IllegalBlockSizeException, BadPaddingException, IOException {
        Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key);

        BASE64Decoder decoder = new BASE64Decoder();
        byte[] raw = decoder.decodeBuffer(encrypted);

        byte[] stringBytes = cipher.doFinal(raw);

        return new String(stringBytes, "UTF8");
    }

    private static String decrypt(String encrypted, String strKey) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException,
            IllegalBlockSizeException, BadPaddingException, IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] encodedKey = decoder.decodeBuffer(strKey);
        Key key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "DES");
        return decrypt(encrypted, key);
    }
}
