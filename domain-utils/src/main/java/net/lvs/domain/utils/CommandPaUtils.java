package net.lvs.domain.utils;

/**
 * 27/04/2022 12:00
 * Phan Hoàng Kiệt
 */

public class CommandPaUtils {
    public static final String CREATE_DOMAIN_VN = "register_domain_vietnam";// đăng ký domain Việt Nam
    public static final String CREATE_DOMAIN_INTERNATIONAL = "register_domain_quocte";// đăng ký domain quốc tế
    public static final String RENEW_DOMAIN = "renew_domain";//gia hạn domain
    public static final String CHANGE_PASSWORD_DOMAIN = "change_password_domain"; // thay đổi passwword domain
    public static final String CHANGE_DNS_DOMAIN_VN = "change_dns_domain_vietnam"; // thay đổi DNS Việt Nam
    public static final String CHANGE_DNS_DOMAIN_INTERNATIONAL = "change_dns_domain_quocte"; // thay đổi DNS quốc tế
    public static final String GET_DATE_DOMAIN = "get_date_domain";// lấy ngày đăng ký và ngáy hết hạn sử dụng tên miền
    public static final String CHECK_WHOIS = "check_whois";// kiểm tra tên miền có tồn tại
    public static final String GET_WHOIS = "get_whois";// lấy thông tin whois của tên miền
    public static final String CHECK_ACCOUNT_STILL_AGENCY = "check_account_still";// kiểm tra số tiền còn lại của đại lý
    public static final String CHECK_ACCOUNT_TOTAL_AGENCY = "check_account_total";//kiểm tra số tiền tổng nạp của đại lý
    public static final String GET_INFO_DOMAIN = "get_info_domain";// lấy thông tin domain- hiện tại chỉ hỗ trợ domain quốc tế
    public static final String UPDATE_INFO_DOMAIN = "set_info_domain"; // cập nhật thông tin của tên miền , hiện tại chỉ hỗ trợ domain quốc tế
    public static final String CHECK_DOMAIN_OF_AGENCY = "check_domain";// check domain có thuộc quản lý của đại lý không ?
}
