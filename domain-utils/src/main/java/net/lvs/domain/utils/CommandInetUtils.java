package net.lvs.domain.utils;

/**
 * 14:22 22/04/2022
 * Admin
 */

public class CommandInetUtils {

    //==============DOMAIN=================================================
    public static final String CREATE_DOMAIN = "/api/rms/v1/domain/create";
    public static final String RENEW_DOMAIN = "/api/rms/v1/domain/renew";
    public static final String CHECK_DOMAIN = "/api/rms/v1/domain/checkavailable";
    public static final String UPDATE_DNS = "/api/rms/v1/domain/updatedns";
    public static final String UPDATE_DNS_CHILD = "/api/rms/v1/domain/updatechilddns";
    public static final String CHECK_DOMAIN_PUNY = "/api/rms/v1/domain/validateidnname";
    public static final String LIST_PROVINCE = "/api/rms/v1/category/provincelist";
    public static final String GET_INFO_DOMAIN = "/api/rms/v1/domain/detail";
    public static final String GET_RECORD_DOMAIN = "/api/rms/v1/domain/getrecord";
    public static final String UPDATE_RECORD_DOMAIN = "/api/rms/v1/domain/updaterecord";
    public static final String RESEND_EMAIL_VERIFICATION_DOMAIN = "/api/rms/v1/domain/resendemailverification";
    public static final String CHANGE_AUTHCODE_DOMAIN = "/api/rms/v1/domain/changeauthcode";
    public static final String UPLOAD_CONTRACT = "/api/rms/v1/domain/uploadcontract";



    public static final String PRIVACY_PROTECTION = "/api/rms/v1/domain/privacyprotection";
    public static final String CHECK_WHOIS = "/api/public/whois/v1/whois/directly";
    public static final String INFO_DOMAIN = "/api/rms/v1/domain/search";
    public static final String CHANGE_PASSWORD_DOMAIN = "/api/rms/v1/domain/changepassword";
    public static final String CHECK_LOOKUP = "/api/public/nslookup/v1/nslookup/lookup";


    //==============DNSSEC=================================================
    public static final String ENABLE_DNSSEC = "/api/rms/v1/domain/enablednssec";
    public static final String SYNC_DNSSEC = " /api/rms/v1/domain/syncdnssec";
    public static final String DISABLE_DNSSEC = "/api/rms/v1/domain/disablednssec";
    public static final String CREATE_DNSSEC_MANUAL = "/api/rms/v1/domain/creatednssecmanual";
    public static final String UPDATE_DNSSEC_MANUAL = "/api/rms/v1/domain/updatednssecmanual";
    public static final String DELETE_DNSSEC_MANUAL = "/api/rms/v1/domain/deletednssecmanual";


    //==============ACCOUNT=================================================
    public static final String GET_ACCOUNT_BY_ID = "/api/rms/v1/customer/get";
    public static final String GET_ACCOUNT_BY_EMAIL = "/api/rms/v1/customer/getbyemail";
    public static final String CHANGE_PASSWORD_ACCOUNT = "/api/rms/v1/customer/changepassword";
    public static final String FORGET_PASSWORD_ACCOUNT = "/api/rms/v1/customer/forgotpassword";
    public static final String CREATE_ACCOUNT = "/api/rms/v1/customer/create";
    public static final String UPDATE_ACCOUNT = "/api/rms/v1/customer/update";
    public static final String SUSPEND_ACCOUNT = "/api/rms/v1/customer/suspend";
    public static final String ACTIVE_ACCOUNT = "/api/rms/v1/customer/active";

    //==============CONTACT=================================================
    public static final String UPLOAD_ID_NUMBER = "/api/rms/v1/contact/uploadidnumber";

    //==============SUFFIX=================================================
    public static final String LIST_SUFFIX = "/api/rms/v1/suffix/list";

    //==============LOG DOMAIN=================================================
    public static final String SEARCH_LOG_DOMAIN = "/api/rms/v1/logdomain/search";

}
