package net.lvs.domain.wrapper;


import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
public class ObjectResponseWrapper extends ResponseWrapper<Object>{
}
