package net.lvs.domain.model.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 29/04/2022 10:30
 * Phan Hoàng Kiệt
 */

public class ReturnTextModel {
    private String owner_name;
    private String owner_company;
    private String owner_email;
    private String owner_address;
    private String owner_city;
    private String owner_zipcode;
    private String owner_country;
    private String owner_phone;
    private String owner_phoneext;
    private String owner_fax;
    private String admin_name;
    private String admin_company;
    private String admin_email;
    private String admin_address;
    private String admin_city;
    private String admin_zipcode;
    private String admin_country;
    private String admin_phone;
    private String admin_phoneext;
    private String admin_fax;
    private String tech_name;
    private String tech_company;
    private String tech_email;
    private String tech_address;
    private String tech_city;
    private String tech_zipcode;
    private String tech_country;
    private String tech_phone;
    private String tech_phoneext;
    private String tech_fax;
    private String billing_name;
    private String billing_company;
    private String billing_email;
    private String billing_address;
    private String billing_city;
    private String billing_zipcode;
    private String billing_country;
    private String billing_phone;
    private String billing_phoneext;
    private String billing_fax;
    private List<String> dns = new ArrayList<>();
    private String owner_signature;


    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getOwner_company() {
        return owner_company;
    }

    public void setOwner_company(String owner_company) {
        this.owner_company = owner_company;
    }

    public String getOwner_email() {
        return owner_email;
    }

    public void setOwner_email(String owner_email) {
        this.owner_email = owner_email;
    }

    public String getOwner_address() {
        return owner_address;
    }

    public void setOwner_address(String owner_address) {
        this.owner_address = owner_address;
    }

    public String getOwner_city() {
        return owner_city;
    }

    public void setOwner_city(String owner_city) {
        this.owner_city = owner_city;
    }

    public String getOwner_zipcode() {
        return owner_zipcode;
    }

    public void setOwner_zipcode(String owner_zipcode) {
        this.owner_zipcode = owner_zipcode;
    }

    public String getOwner_country() {
        return owner_country;
    }

    public void setOwner_country(String owner_country) {
        this.owner_country = owner_country;
    }

    public String getOwner_phone() {
        return owner_phone;
    }

    public void setOwner_phone(String owner_phone) {
        this.owner_phone = owner_phone;
    }

    public String getOwner_phoneext() {
        return owner_phoneext;
    }

    public void setOwner_phoneext(String owner_phoneext) {
        this.owner_phoneext = owner_phoneext;
    }

    public String getOwner_fax() {
        return owner_fax;
    }

    public void setOwner_fax(String owner_fax) {
        this.owner_fax = owner_fax;
    }

    public String getAdmin_name() {
        return admin_name;
    }

    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }

    public String getAdmin_company() {
        return admin_company;
    }

    public void setAdmin_company(String admin_company) {
        this.admin_company = admin_company;
    }

    public String getAdmin_email() {
        return admin_email;
    }

    public void setAdmin_email(String admin_email) {
        this.admin_email = admin_email;
    }

    public String getAdmin_address() {
        return admin_address;
    }

    public void setAdmin_address(String admin_address) {
        this.admin_address = admin_address;
    }

    public String getAdmin_city() {
        return admin_city;
    }

    public void setAdmin_city(String admin_city) {
        this.admin_city = admin_city;
    }

    public String getAdmin_zipcode() {
        return admin_zipcode;
    }

    public void setAdmin_zipcode(String admin_zipcode) {
        this.admin_zipcode = admin_zipcode;
    }

    public String getAdmin_country() {
        return admin_country;
    }

    public void setAdmin_country(String admin_country) {
        this.admin_country = admin_country;
    }

    public String getAdmin_phone() {
        return admin_phone;
    }

    public void setAdmin_phone(String admin_phone) {
        this.admin_phone = admin_phone;
    }

    public String getAdmin_phoneext() {
        return admin_phoneext;
    }

    public void setAdmin_phoneext(String admin_phoneext) {
        this.admin_phoneext = admin_phoneext;
    }

    public String getAdmin_fax() {
        return admin_fax;
    }

    public void setAdmin_fax(String admin_fax) {
        this.admin_fax = admin_fax;
    }

    public String getTech_name() {
        return tech_name;
    }

    public void setTech_name(String tech_name) {
        this.tech_name = tech_name;
    }

    public String getTech_company() {
        return tech_company;
    }

    public void setTech_company(String tech_company) {
        this.tech_company = tech_company;
    }

    public String getTech_email() {
        return tech_email;
    }

    public void setTech_email(String tech_email) {
        this.tech_email = tech_email;
    }

    public String getTech_address() {
        return tech_address;
    }

    public void setTech_address(String tech_address) {
        this.tech_address = tech_address;
    }

    public String getTech_city() {
        return tech_city;
    }

    public void setTech_city(String tech_city) {
        this.tech_city = tech_city;
    }

    public String getTech_zipcode() {
        return tech_zipcode;
    }

    public void setTech_zipcode(String tech_zipcode) {
        this.tech_zipcode = tech_zipcode;
    }

    public String getTech_country() {
        return tech_country;
    }

    public void setTech_country(String tech_country) {
        this.tech_country = tech_country;
    }

    public String getTech_phone() {
        return tech_phone;
    }

    public void setTech_phone(String tech_phone) {
        this.tech_phone = tech_phone;
    }

    public String getTech_phoneext() {
        return tech_phoneext;
    }

    public void setTech_phoneext(String tech_phoneext) {
        this.tech_phoneext = tech_phoneext;
    }

    public String getTech_fax() {
        return tech_fax;
    }

    public void setTech_fax(String tech_fax) {
        this.tech_fax = tech_fax;
    }

    public String getBilling_name() {
        return billing_name;
    }

    public void setBilling_name(String billing_name) {
        this.billing_name = billing_name;
    }

    public String getBilling_company() {
        return billing_company;
    }

    public void setBilling_company(String billing_company) {
        this.billing_company = billing_company;
    }

    public String getBilling_email() {
        return billing_email;
    }

    public void setBilling_email(String billing_email) {
        this.billing_email = billing_email;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(String billing_address) {
        this.billing_address = billing_address;
    }

    public String getBilling_city() {
        return billing_city;
    }

    public void setBilling_city(String billing_city) {
        this.billing_city = billing_city;
    }

    public String getBilling_zipcode() {
        return billing_zipcode;
    }

    public void setBilling_zipcode(String billing_zipcode) {
        this.billing_zipcode = billing_zipcode;
    }

    public String getBilling_country() {
        return billing_country;
    }

    public void setBilling_country(String billing_country) {
        this.billing_country = billing_country;
    }

    public String getBilling_phone() {
        return billing_phone;
    }

    public void setBilling_phone(String billing_phone) {
        this.billing_phone = billing_phone;
    }

    public String getBilling_phoneext() {
        return billing_phoneext;
    }

    public void setBilling_phoneext(String billing_phoneext) {
        this.billing_phoneext = billing_phoneext;
    }

    public String getBilling_fax() {
        return billing_fax;
    }

    public void setBilling_fax(String billing_fax) {
        this.billing_fax = billing_fax;
    }

    public List<String> getDns() {
        return dns;
    }

    public void setDns(List<String> dns) {
        this.dns = dns;
    }

    public String getOwner_signature() {
        return owner_signature;
    }

    public void setOwner_signature(String owner_signature) {
        this.owner_signature = owner_signature;
    }
}
