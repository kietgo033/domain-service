package net.lvs.domain.model.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Huy_Nguyen on 28/06/2019.
 */

public class ResultWhoIsModel {
    public static final String NOT_EXIST = "Domain does not exist";
    public final static String NO_MATCH = "Domain does not exist";
    public final static String ERROR = "error";

    private String code;
    private String message;
    private String lastUpdated;
    private float totalQuery;
    private String domainName;
    private String registrar;
    private List<String> nameServer = new ArrayList<>();
    private List<String> status = new ArrayList<>();
    private String creationDate;
    private String expirationDate;
    private String registrantName;
    private String DNSSEC;

    // Getter Methods

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public float getTotalQuery() {
        return totalQuery;
    }

    public String getDomainName() {
        return domainName;
    }

    public String getRegistrar() {
        return registrar;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public String getRegistrantName() {
        return registrantName;
    }

    public String getDNSSEC() {
        return DNSSEC;
    }

    // Setter Methods

    public void setCode(String code) {
        this.code = code;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setTotalQuery(float totalQuery) {
        this.totalQuery = totalQuery;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }


    public void setRegistrantName(String registrantName) {
        this.registrantName = registrantName;
    }

    public void setDNSSEC(String DNSSEC) {
        this.DNSSEC = DNSSEC;
    }

    public List<String> getNameServer() {
        if (nameServer == null || nameServer.size() == 0) {
            nameServer = new ArrayList<>();
        }
        return nameServer;
    }

    public void setNameServer(List<String> nameServer) {
        this.nameServer = nameServer;
    }

    public List<String> getStatus() {
        if (status == null || status.size() == 0) {
            status = new ArrayList<>();
        }
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }
}