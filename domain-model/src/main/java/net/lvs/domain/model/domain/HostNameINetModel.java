package net.lvs.domain.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * 14:35 22/04/2022
 * Admin
 */

public class HostNameINetModel {
    @SerializedName("hostname")
    @JsonProperty(value = "hostname")
    private String hostName;

    public HostNameINetModel() {
    }

    public HostNameINetModel(String hostName) {
        this.hostName = hostName;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
}
