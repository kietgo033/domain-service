package net.lvs.domain.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 29/04/2022 15:17
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultFullPaModel {
    @JsonProperty(value = "Command")
    @SerializedName(value = "Command")
    private String command;
    @JsonProperty(value = "ReturnCode")
    @SerializedName(value = "ReturnCode")
    private int returnCode;
    @JsonProperty(value = "ReturnText")
    @SerializedName(value = "ReturnText")
    private String returnText;

    // Nếu: return_full=1
    @JsonProperty(value = "trans_id")
    @SerializedName(value = "trans_id")
    private String trans_id;
    @JsonProperty(value = "trans_date")
    @SerializedName(value = "trans_date")
    private String trans_date;
    @JsonProperty(value = "trans_command")
    @SerializedName(value = "trans_command")
    private String trans_command;
    @JsonProperty(value = "trans_domain")
    @SerializedName(value = "trans_domain")
    private String trans_domain;
    @JsonProperty(value = "trans_note")
    @SerializedName(value = "trans_note")
    private String trans_note;
    @JsonProperty(value = "trans_money")
    @SerializedName(value = "trans_money")
    private String trans_money;
    @JsonProperty(value = "trans_accountstill")
    @SerializedName(value = "trans_accountstill")
    private String trans_accountStill;
}
