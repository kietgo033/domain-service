package net.lvs.domain.model.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 14:34 22/04/2022
 * Admin
 */

public class ResultDomainModel {

    public final static String CHECK_OK = "available";
    public final static String CHECK_FAIL = "notavailable";
    public final static String ERROR = "error";
    public final static String SUCCESS = "success";
    public final static String ACTIVE = "active";


    private String id;
    private String code;
    private String facilityId;
    private String domain;
    private String roidType;
    private int orgId;
    private String customerId;
    private String status;
    private String issueDate;
    private String expireDate;
    private String registrar;
    private int trialDay;
    private int period;
    private boolean ownerView;
    private boolean renewTrial;
    private String message;
    private String contractToken;
    private TransactionINetModel transaction;
    private List<HostNameINetModel> nsList;


    //////bổ sung------////////
    private String name;
    private String authCode;
    private boolean privacyProtection;
    private boolean removeRecord;
    private String registrant;
    private String password;
    private String contract;
    private String verifyStatus;
    private List<HostNameModel> hostList;
    private List<ContactINetModel> contacts;
    private List<RecordInetModel> recordList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public boolean isPrivacyProtection() {
        return privacyProtection;
    }

    public void setPrivacyProtection(boolean privacyProtection) {
        this.privacyProtection = privacyProtection;
    }

    public boolean isRemoveRecord() {
        return removeRecord;
    }

    public void setRemoveRecord(boolean removeRecord) {
        this.removeRecord = removeRecord;
    }

    public String getRegistrant() {
        return registrant;
    }

    public void setRegistrant(String registrant) {
        this.registrant = registrant;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public List<HostNameModel> getHostList() {
        return hostList;
    }

    public void setHostList(List<HostNameModel> hostList) {
        this.hostList = hostList;
    }

    public List<ContactINetModel> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactINetModel> contacts) {
        this.contacts = contacts;
    }

    public List<RecordInetModel> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<RecordInetModel> recordList) {
        this.recordList = recordList;
    }

    //--------Pa domain-----------
    @JsonProperty(value = "Command")
    @SerializedName(value = "Command")
    private String command;
    @JsonProperty(value = "ReturnCode")
    @SerializedName(value = "ReturnCode")
    private int returnCode;
    @JsonProperty(value = "ReturnText")
    @SerializedName(value = "ReturnText")
    private ReturnTextModel returnText;

    // Dùng nhận kết quả khi taoọ và gian hạn domain của PA
    ResultFullPaModel resultFullPaModel;



    public ResultFullPaModel getResultFullPaModel() {
        return resultFullPaModel;
    }

    public void setResultFullPaModel(ResultFullPaModel resultFullPaModel) {
        this.resultFullPaModel = resultFullPaModel;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public ReturnTextModel getReturnText() {
        return returnText;
    }

    public void setReturnText(ReturnTextModel returnText) {
        this.returnText = returnText;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getRoidType() {
        return roidType;
    }

    public void setRoidType(String roidType) {
        this.roidType = roidType;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public int getTrialDay() {
        return trialDay;
    }

    public void setTrialDay(int trialDay) {
        this.trialDay = trialDay;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public boolean isOwnerView() {
        return ownerView;
    }

    public void setOwnerView(boolean ownerView) {
        this.ownerView = ownerView;
    }

    public boolean isRenewTrial() {
        return renewTrial;
    }

    public void setRenewTrial(boolean renewTrial) {
        this.renewTrial = renewTrial;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getContractToken() {
        return contractToken;
    }

    public void setContractToken(String contractToken) {
        this.contractToken = contractToken;
    }

    public TransactionINetModel getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionINetModel transaction) {
        this.transaction = transaction;
    }

    public List<HostNameINetModel> getNsList() {
        return nsList;
    }

    public void setNsList(List<HostNameINetModel> nsList) {
        this.nsList = nsList;
    }
}
