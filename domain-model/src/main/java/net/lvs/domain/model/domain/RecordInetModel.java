package net.lvs.domain.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 04/05/2022 15:05
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordInetModel {
    private String id;
    private String type;
    private String name;
    private String data;
    private String action;
}
