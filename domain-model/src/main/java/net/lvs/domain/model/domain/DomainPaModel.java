package net.lvs.domain.model.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Huy_Nguyen on 7/20/2018.
 */

public class DomainPaModel {
    private String cmd; //lệnh thực thi
    private String username;//userName đại lý
    private String apikey;// API đại lý
    private String domainName;// tên miền không có phần mở rộng -- Ex: .vn .com
    private String domainExt;// phần mở rông tên miền --- Ex: .vn .com
    //thời gian đăng ký
    private String passwordDomain;//mật khẩu quản lý domain
    @SerializedName("for")
    private String forDomain; //đăng ký cho cá nhân hoặc công ty
    private String domainDNS1; // Tên domain chính
    private String domainDNS2;// Tên domain phụ
    private String domainDNS3;
    private String domainDNS4;
    private String domainIP1;
    private String domainIP2;
    private String domainIP3;
    //Thông tin tạo khách hàng
    private String ownerName;//Tên khách hàng(cá nhân hoặc công ty) ---Ex : PA-XXX
    private String ownerID_Number;//Chứng minh nhân dân khách hàng
    private String ownerAddress;// Địa chỉ khách hàng
    private String ownerEmail;// Email khách hàng chính
    private String ownerEmail1;//
    private String ownerEmail2;
    private String ownerPhone;//Số đt của khách hàng
    private String ownerPhone1;
    private String ownerFax;//Fax của khách hàng
    private String ownerTaxCode;//Bắt buộc đối với cty - Mã số thuê của khách hàng
    private String ownerProvince;
    private String ownerCountry;

    //Thông tin user
    private String uiName;//Tên chủ thể (cá nhân - cty ) --- Ex : Nguyễn Phạm Huy
    private String uiID_Number;//Chứng minh nhân
    private String uiTaxCode;
    private String uiAddress;
    private String uiProvince;
    private String uiCountry;
    private String uiEmail;
    private String uiPhone;
    private String uiFax;
    private String uiGender;
    private String uiBirthDate;
    private String uiCompany;
    private String uiPosition;// chức vụ

    //Thông tin người quản lý
    private String adminName;
    private String adminID_Number;
    private String adminPosition;
    private String adminAddress;
    private String adminProvince;
    private String adminCountry;
    private String adminEmail;
    private String adminPhone;
    private String adminFax;
    private String adminGender;
    private String adminBirthdate;
    private String adminCompany;
    private String sendmail;
    private String return_full;
    private String responsetype;
    //renew domain
    private String domainYear;
    private String year;
    private String domain;
    //update domain
    private String owner_name;
    private String owner_company;
    private String owner_email;
    private String owner_address;
    private String owner_city;
    private String owner_country;
    private String owner_phone;
    private String owner_fax;
    private String admin_name;
    private String admin_company;
    private String admin_email;
    private String admin_address;
    private String admin_city;
    private String admin_country;
    private String admin_phone;
    private String admin_fax;
    private String tech_name;
    private String tech_company;
    private String tech_email;
    private String tech_address;
    private String tech_city;
    private String tech_country;
    private String tech_phone;
    private String tech_fax;
    private String billing_name;
    private String billing_company;
    private String billing_email;
    private String billing_address;
    private String billing_city;
    private String billing_country;
    private String billing_phone;
    private String billing_fax;


    public DomainPaModel() {
        this.cmd = null;
        this.username = null;
        this.apikey = null;
        this.domainName = null;
        this.domainExt = null;
        this.passwordDomain = null;
        this.forDomain = null;
        this.domainDNS1 = null;
        this.domainDNS2 = null;
        this.domainDNS3 = null;
        this.domainDNS4 = null;
        this.domainIP1 = null;
        this.domainIP2 = null;
        this.domainIP3 = null;
        this.ownerName = null;
        this.ownerID_Number = null;
        this.ownerAddress = null;
        this.ownerEmail = null;
        this.ownerEmail1 = null;
        this.ownerEmail2 = null;
        this.ownerPhone = null;
        this.ownerPhone1 = null;
        this.ownerFax = null;
        this.ownerTaxCode = null;
        this.ownerCountry = null;
        this.ownerProvince = null;
        this.uiName = null;
        this.uiID_Number = null;
        this.uiTaxCode = null;
        this.uiAddress = null;
        this.uiProvince = null;
        this.uiCountry = null;
        this.uiEmail = null;
        this.uiPhone = null;
        this.uiFax = null;
        this.uiGender = null;
        this.uiBirthDate = null;
        this.uiCompany = null;
        this.uiPosition = null;
        this.adminName = null;
        this.adminID_Number = null;
        this.adminPosition = null;
        this.adminAddress = null;
        this.adminProvince = null;
        this.adminCountry = null;
        this.adminEmail = null;
        this.adminPhone = null;
        this.adminFax = null;
        this.adminGender = null;
        this.adminBirthdate = null;
        this.adminCompany = null;
        this.sendmail = null;
        this.return_full = null;
        this.responsetype = null;
        this.domainYear = null;
        this.year = null;
        this.domain = null;
        this.owner_name = null;
        this.owner_company = null;
        this.owner_email = null;
        this.owner_address = null;
        this.owner_city = null;
        this.owner_country = null;
        this.owner_phone = null;
        this.owner_fax = null;
        this.admin_name = null;
        this.admin_company = null;
        this.admin_email = null;
        this.admin_address = null;
        this.admin_city = null;
        this.admin_country = null;
        this.admin_phone = null;
        this.admin_fax = null;
        this.tech_name = null;
        this.tech_company = null;
        this.tech_email = null;
        this.tech_address = null;
        this.tech_city = null;
        this.tech_country = null;
        this.tech_phone = null;
        this.tech_fax = null;
        this.billing_name = null;
        this.billing_company = null;
        this.billing_email = null;
        this.billing_address = null;
        this.billing_city = null;
        this.billing_country = null;
        this.billing_phone = null;
        this.billing_fax = null;
    }

    //GET-SET===========================================================================================================
    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainExt() {
        return domainExt;
    }

    public void setDomainExt(String domainExt) {
        this.domainExt = domainExt;
    }

    public String getPasswordDomain() {
        return passwordDomain;
    }

    public void setPasswordDomain(String passwordDomain) {
        this.passwordDomain = passwordDomain;
    }

    public String getForDomain() {
        return forDomain;
    }

    public void setForDomain(String forDomain) {
        this.forDomain = forDomain;
    }

    public String getDomainDNS1() {
        return domainDNS1;
    }

    public void setDomainDNS1(String domainDNS1) {
        this.domainDNS1 = domainDNS1;
    }

    public String getDomainDNS2() {
        return domainDNS2;
    }

    public void setDomainDNS2(String domainDNS2) {
        this.domainDNS2 = domainDNS2;
    }

    public String getDomainDNS3() {
        return domainDNS3;
    }

    public void setDomainDNS3(String domainDNS3) {
        this.domainDNS3 = domainDNS3;
    }

    public String getDomainDNS4() {
        return domainDNS4;
    }

    public void setDomainDNS4(String domainDNS4) {
        this.domainDNS4 = domainDNS4;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerAddress() {
        return ownerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getOwnerEmail1() {
        return ownerEmail1;
    }

    public void setOwnerEmail1(String ownerEmail1) {
        this.ownerEmail1 = ownerEmail1;
    }

    public String getOwnerEmail2() {
        return ownerEmail2;
    }

    public void setOwnerEmail2(String ownerEmail2) {
        this.ownerEmail2 = ownerEmail2;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public String getOwnerPhone1() {
        return ownerPhone1;
    }

    public void setOwnerPhone1(String ownerPhone1) {
        this.ownerPhone1 = ownerPhone1;
    }

    public String getOwnerFax() {
        return ownerFax;
    }

    public void setOwnerFax(String ownerFax) {
        this.ownerFax = ownerFax;
    }

    public String getOwnerTaxCode() {
        return ownerTaxCode;
    }

    public void setOwnerTaxCode(String ownerTaxCode) {
        this.ownerTaxCode = ownerTaxCode;
    }

    public String getOwnerProvince() {
        return ownerProvince;
    }

    public void setOwnerProvince(String ownerProvince) {
        this.ownerProvince = ownerProvince;
    }

    public String getOwnerCountry() {
        return ownerCountry;
    }

    public void setOwnerCountry(String ownerCountry) {
        this.ownerCountry = ownerCountry;
    }

    public String getUiName() {
        return uiName;
    }

    public void setUiName(String uiName) {
        this.uiName = uiName;
    }

    public String getUiTaxCode() {
        return uiTaxCode;
    }

    public void setUiTaxCode(String uiTaxCode) {
        this.uiTaxCode = uiTaxCode;
    }

    public String getUiAddress() {
        return uiAddress;
    }

    public void setUiAddress(String uiAddress) {
        this.uiAddress = uiAddress;
    }

    public String getUiProvince() {
        return uiProvince;
    }

    public void setUiProvince(String uiProvince) {
        this.uiProvince = uiProvince;
    }

    public String getUiCountry() {
        return uiCountry;
    }

    public void setUiCountry(String uiCountry) {
        this.uiCountry = uiCountry;
    }

    public String getUiEmail() {
        return uiEmail;
    }

    public void setUiEmail(String uiEmail) {
        this.uiEmail = uiEmail;
    }

    public String getUiPhone() {
        return uiPhone;
    }

    public void setUiPhone(String uiPhone) {
        this.uiPhone = uiPhone;
    }

    public String getUiFax() {
        return uiFax;
    }

    public void setUiFax(String uiFax) {
        this.uiFax = uiFax;
    }

    public String getUiGender() {
        return uiGender;
    }

    public void setUiGender(String uiGender) {
        this.uiGender = uiGender;
    }

    public String getUiBirthDate() {
        return uiBirthDate;
    }

    public void setUiBirthDate(String uiBirthDate) {
        this.uiBirthDate = uiBirthDate;
    }

    public String getUiCompany() {
        return uiCompany;
    }

    public void setUiCompany(String uiCompany) {
        this.uiCompany = uiCompany;
    }

    public String getUiPosition() {
        return uiPosition;
    }

    public void setUiPosition(String uiPosition) {
        this.uiPosition = uiPosition;
    }

    public String getAdminPosition() {
        return adminPosition;
    }

    public void setAdminPosition(String adminPosition) {
        this.adminPosition = adminPosition;
    }

    public String getAdminID_Number() {
        return adminID_Number;
    }

    public void setAdminID_Number(String adminID_Number) {
        this.adminID_Number = adminID_Number;
    }

    public String getAdminAddress() {
        return adminAddress;
    }

    public void setAdminAddress(String adminAddress) {
        this.adminAddress = adminAddress;
    }

    public String getAdminProvince() {
        return adminProvince;
    }

    public void setAdminProvince(String adminProvince) {
        this.adminProvince = adminProvince;
    }

    public String getAdminCountry() {
        return adminCountry;
    }

    public void setAdminCountry(String adminCountry) {
        this.adminCountry = adminCountry;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getAdminPhone() {
        return adminPhone;
    }

    public void setAdminPhone(String adminPhone) {
        this.adminPhone = adminPhone;
    }

    public String getAdminFax() {
        return adminFax;
    }

    public void setAdminFax(String adminFax) {
        this.adminFax = adminFax;
    }

    public String getAdminGender() {
        return adminGender;
    }

    public String getAdminBirthdate() {
        return adminBirthdate;
    }

    public void setAdminBirthdate(String adminBirthdate) {
        this.adminBirthdate = adminBirthdate;
    }

    public void setAdminGender(String adminGender) {
        this.adminGender = adminGender;
    }

    public String getAdminCompany() {
        return adminCompany;
    }

    public void setAdminCompany(String adminCompany) {
        this.adminCompany = adminCompany;
    }

    public String getUiID_Number() {
        return uiID_Number;
    }

    public void setUiID_Number(String uiID_Number) {
        this.uiID_Number = uiID_Number;
    }

    public String getReturn_full() {
        return return_full;
    }

    public void setReturn_full(String return_full) {
        this.return_full = return_full;
    }

    public String getResponsetype() {
        return responsetype;
    }

    public void setResponsetype(String responsetype) {
        this.responsetype = responsetype;
    }

    public String getDomainYear() {
        return domainYear;
    }

    public void setDomainYear(String domainYear) {
        this.domainYear = domainYear;
    }

    public String getOwnerID_Number() {
        return ownerID_Number;
    }

    public void setOwnerID_Number(String ownerID_Number) {
        this.ownerID_Number = ownerID_Number;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDomainIP1() {
        return domainIP1;
    }

    public void setDomainIP1(String domainIP1) {
        this.domainIP1 = domainIP1;
    }

    public String getDomainIP2() {
        return domainIP2;
    }

    public void setDomainIP2(String domainIP2) {
        this.domainIP2 = domainIP2;
    }

    public String getDomainIP3() {
        return domainIP3;
    }

    public void setDomainIP3(String domainIP3) {
        this.domainIP3 = domainIP3;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getOwner_company() {
        return owner_company;
    }

    public void setOwner_company(String owner_company) {
        this.owner_company = owner_company;
    }

    public String getOwner_email() {
        return owner_email;
    }

    public void setOwner_email(String owner_email) {
        this.owner_email = owner_email;
    }

    public String getOwner_address() {
        return owner_address;
    }

    public void setOwner_address(String owner_address) {
        this.owner_address = owner_address;
    }

    public String getOwner_city() {
        return owner_city;
    }

    public void setOwner_city(String owner_city) {
        this.owner_city = owner_city;
    }

    public String getOwner_country() {
        return owner_country;
    }

    public void setOwner_country(String owner_country) {
        this.owner_country = owner_country;
    }

    public String getOwner_phone() {
        return owner_phone;
    }

    public void setOwner_phone(String owner_phone) {
        this.owner_phone = owner_phone;
    }

    public String getOwner_fax() {
        return owner_fax;
    }

    public void setOwner_fax(String owner_fax) {
        this.owner_fax = owner_fax;
    }

    public String getAdmin_name() {
        return admin_name;
    }

    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }

    public String getAdmin_company() {
        return admin_company;
    }

    public void setAdmin_company(String admin_company) {
        this.admin_company = admin_company;
    }

    public String getAdmin_email() {
        return admin_email;
    }

    public void setAdmin_email(String admin_email) {
        this.admin_email = admin_email;
    }

    public String getAdmin_address() {
        return admin_address;
    }

    public void setAdmin_address(String admin_address) {
        this.admin_address = admin_address;
    }

    public String getAdmin_city() {
        return admin_city;
    }

    public void setAdmin_city(String admin_city) {
        this.admin_city = admin_city;
    }

    public String getAdmin_country() {
        return admin_country;
    }

    public void setAdmin_country(String admin_country) {
        this.admin_country = admin_country;
    }

    public String getAdmin_phone() {
        return admin_phone;
    }

    public void setAdmin_phone(String admin_phone) {
        this.admin_phone = admin_phone;
    }

    public String getAdmin_fax() {
        return admin_fax;
    }

    public void setAdmin_fax(String admin_fax) {
        this.admin_fax = admin_fax;
    }

    public String getTech_name() {
        return tech_name;
    }

    public void setTech_name(String tech_name) {
        this.tech_name = tech_name;
    }

    public String getTech_company() {
        return tech_company;
    }

    public void setTech_company(String tech_company) {
        this.tech_company = tech_company;
    }

    public String getTech_email() {
        return tech_email;
    }

    public void setTech_email(String tech_email) {
        this.tech_email = tech_email;
    }

    public String getTech_address() {
        return tech_address;
    }

    public void setTech_address(String tech_address) {
        this.tech_address = tech_address;
    }

    public String getTech_city() {
        return tech_city;
    }

    public void setTech_city(String tech_city) {
        this.tech_city = tech_city;
    }

    public String getTech_country() {
        return tech_country;
    }

    public void setTech_country(String tech_country) {
        this.tech_country = tech_country;
    }

    public String getTech_phone() {
        return tech_phone;
    }

    public void setTech_phone(String tech_phone) {
        this.tech_phone = tech_phone;
    }

    public String getTech_fax() {
        return tech_fax;
    }

    public void setTech_fax(String tech_fax) {
        this.tech_fax = tech_fax;
    }

    public String getBilling_name() {
        return billing_name;
    }

    public void setBilling_name(String billing_name) {
        this.billing_name = billing_name;
    }

    public String getBilling_company() {
        return billing_company;
    }

    public void setBilling_company(String billing_company) {
        this.billing_company = billing_company;
    }

    public String getBilling_email() {
        return billing_email;
    }

    public void setBilling_email(String billing_email) {
        this.billing_email = billing_email;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(String billing_address) {
        this.billing_address = billing_address;
    }

    public String getBilling_city() {
        return billing_city;
    }

    public void setBilling_city(String billing_city) {
        this.billing_city = billing_city;
    }

    public String getBilling_country() {
        return billing_country;
    }

    public void setBilling_country(String billing_country) {
        this.billing_country = billing_country;
    }

    public String getBilling_phone() {
        return billing_phone;
    }

    public void setBilling_phone(String billing_phone) {
        this.billing_phone = billing_phone;
    }

    public String getBilling_fax() {
        return billing_fax;
    }

    public void setBilling_fax(String billing_fax) {
        this.billing_fax = billing_fax;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getSendmail() {
        return sendmail;
    }

    public void setSendmail(String sendmail) {
        this.sendmail = sendmail;
    }

}
