package net.lvs.domain.model.domain;

/**
 * 14:42 22/04/2022
 * Admin
 */

public class ContactINetModel {
    public final static String ADMIN = "admin";
    public final static String TECHNIQUE = "technique";
    public final static String REGISTRANT = "registrant";
    public final static String BILLING = "billing";


    private String id;
    private String orgId;
    private String emailLogin;
    private String fullname;
    private boolean organization;
    private String organizationName;
    private String gender;
    private String birthday;
    private String email;
    private String password;
    private String country;
    private String province;
    private String address;
    private String phone;
    private String type;
    private String taxCode;
    private String idNumber;
    private String createdDate;


    private String status;
    private String message;

    public ContactINetModel() {

    }


    public ContactINetModel(ContactINetModel contactINetModel, String type) {
        this.fullname = contactINetModel.getFullname();
        this.emailLogin = contactINetModel.getEmailLogin();
        this.email = contactINetModel.getEmail();
        this.organization = contactINetModel.isOrganization();
        this.country = contactINetModel.getCountry();
        this.province = contactINetModel.getProvince();
        this.address = contactINetModel.getAddress();
        this.phone = contactINetModel.getPhone();
        this.type = type;
        this.idNumber = contactINetModel.getIdNumber();
        this.birthday = contactINetModel.getBirthday();
        this.taxCode = contactINetModel.getTaxCode();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getEmailLogin() {
        return emailLogin;
    }

    public void setEmailLogin(String emailLogin) {
        this.emailLogin = emailLogin;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isOrganization() {
        return organization;
    }

    public void setOrganization(boolean organization) {
        this.organization = organization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedDate() { return createdDate; }

    public void setCreatedDate(String createdDate) { this.createdDate = createdDate; }
}
