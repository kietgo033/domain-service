package net.lvs.domain.dto.inet;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 26/04/2022 16:39
 * Phan Hoàng Kiệt
 */
@Data
public class InetEmailDTO {
    @SerializedName(value = "email")
    private String email;
    public InetEmailDTO(String email){
        this.email=email;
    }
}
