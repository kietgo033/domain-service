package net.lvs.domain.dto.inet;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 09:13 25/04/2022
 * Admin
 */
@Data
@AllArgsConstructor
public class InetCheckExistDomainDTO {
    @JsonProperty(value = "name")
    @SerializedName(value = "name")
    private String name;
}
