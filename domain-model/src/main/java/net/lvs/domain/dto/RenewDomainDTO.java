package net.lvs.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 04/05/2022 09:07
 * Phan Hoàng Kiệt
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RenewDomainDTO {
    private String id;
    private int period;
    private String domain;
    private String year;
    @JsonProperty(value = "sentmail")
    @SerializedName(value = "sentmail")
    private String sentMail;
}
