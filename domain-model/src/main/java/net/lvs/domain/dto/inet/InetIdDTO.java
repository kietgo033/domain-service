package net.lvs.domain.dto.inet;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 26/04/2022 08:58
 * Phan Hoàng Kiệt
 */

@Data
public class InetIdDTO {
    @SerializedName(value = "id")
    private String id;
    public InetIdDTO(String id){
        this.id=id;
    }
}