package net.lvs.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResponseCustomBodyDTO {
    private Object data;
    private int status;
    private String message;
}
