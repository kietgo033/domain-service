package net.lvs.domain.dto.inet;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 25/04/2022 16:09
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
public class InetRenewDomainDTO {
    private String id;
    private int period;
}
