package net.lvs.domain.dto.inet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 04/05/2022 16:18
 * Phan Hoàng Kiệt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InetChangeAuthCodeDTO {
    private String id;
    private String name;
}
