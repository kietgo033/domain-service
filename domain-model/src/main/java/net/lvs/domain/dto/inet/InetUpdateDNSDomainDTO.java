package net.lvs.domain.dto.inet;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import net.lvs.domain.model.domain.HostNameINetModel;
import net.lvs.domain.model.domain.HostNameModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 09:13 25/04/2022
 * Admin
 */
@Data
public class InetUpdateDNSDomainDTO {
    @SerializedName(value = "id")
    private String id;
    @SerializedName(value = "nsList")
    private List<HostNameINetModel> nsList;

}
