package net.lvs.domain.dto.inet;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 28/04/2022 09:22
 * Phan Hoàng Kiệt
 */
@Data
public class InetChangePasswordDTO {
    private String id;
    private String password;
}
