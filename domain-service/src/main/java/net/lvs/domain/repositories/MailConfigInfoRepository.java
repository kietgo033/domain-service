package net.lvs.domain.repositories;

import net.lvs.domain.documents.MailConfigInfoDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailConfigInfoRepository extends MongoRepository<MailConfigInfoDocument, String> {
    MailConfigInfoDocument findByKey(String key);
}
