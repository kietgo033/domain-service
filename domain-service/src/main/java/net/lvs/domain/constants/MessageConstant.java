package net.lvs.domain.constants;

public class MessageConstant {
    //
    public static final String EX_HOST_UNKNOW="Host unknow";
    public static final String TRY_AFTER="please try after some time!";
    //Member response message
    public static final String MEMBER_GET_SUCCESS="Get member success";
    public static final String MEMBER_UPDATE_SUCCESS="Update member success";
    //Domain response message
    public static final String DOMAIN_GET_SUCCESS="Get message success";
    public static final String DOMAIN_CHECK_STATUS="Check status success";
    public static final String DOMAIN_ENABLE_SUCCESS="Enable domain success";
    public static final String DOMAIN_DISABLE_SUCCESS="Disable domain success";
    public static final String DOMAIN_CHANGE_NAME_SUCCESS="Change domain success";
    public static final String DOMAIN_GET_RESOURCE_SUCCESS="Get domain resource success";
    public static final String DOMAIN_GET_STORAGE_SUCCESS="Get domain storage success";
    //Group response message
    public static final String GROUP_GET_SUCCESS="Get group success";
    public static final String GROUP_CREATE_SUCCESS="Create group success";
    public static final String GROUP_REMOVE_SUCCESS="Remove group success";
    public static final String GROUP_GET_MEMBER_SUCCESS="Get member in group success";
    public static final String GROUP_REMOVE_MEMBER_SUCCESS="Remove member in group success";
    public static final String GROUP_ADD_MEMBER_SUCCESS="Add member in group success";
    //Mail response message
    public static final String MAIL_CREATE_SUCCESS="Create mail success";
    public static final String MAIL_CHECK_EXISTED="Check mail existed success";
    public static final String MAIL_DELETE_MAIL="Delete mail success";
    //Message response message
    public static final String MESSAGE_ADD_LOCATION_SUCCESS="Add location for message success";
    //User response message
    public static final String USER_CREATE_SUCCESS="Create user success";
    public static final String USER_DELETE_SUCCESS="Delete user success";
    public static final String USER_CHANGE_PASSWORD_SUCCESS="Change user password success";
    public static final String USER_CHANGE_FULLNAME_SUCCESS="Change user fullname success";
    public static final String USER_CHANGE_ADMIN_PASS_SUCCESS="Change admin password success";
}
