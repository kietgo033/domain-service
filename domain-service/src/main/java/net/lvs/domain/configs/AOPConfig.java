package net.lvs.domain.configs;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
@Aspect
public class AOPConfig {
    private Logger logger = LoggerFactory.getLogger(AOPConfig.class);

    @Before("execution(* net.lvs.domain.apis.domain.*.*(..))")
    public void beforeResource(JoinPoint joinPoint){
        logger.info(" <<--- START RUN RESOURCE" + joinPoint.toString()+" --->>");
    }

    @After("execution(* net.lvs.domain.apis.domain.*.*(..))")
    public void AfterResource(JoinPoint joinPoint){
        logger.info(" <<--- END RUN RESOURCE" + joinPoint.toString()+" --->>");
    }
}
