package net.lvs.domain.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@EnableSwagger2
@Configuration
public class SwaggerConfig implements WebMvcConfigurer {
    @Bean
    public Docket domainDocket() {

        List<SecurityScheme> listScheme= new ArrayList();
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("domain-api-1.0").select()
                .apis(RequestHandlerSelectors.basePackage("net.lvs.domain.apis.domain"))
                .paths(PathSelectors.regex("/.*"))
                .build()
                .apiInfo(domainEndPointsInfo());
    }
    private ApiInfo domainEndPointsInfo() {
        return new ApiInfoBuilder().title("DOMAIN Server - INET-PA - REST API")
                .description("\n" +
                        "\tHTTP Code\n" +
                        "\t\t200 - Success\n" +
                        "\t\t401 - Unauthorized \n" +
                        "\t\t400 - Bad Request (some parameters may contain invalid values)\n" +
                        "\t\t500 - Server error\n" +
                        "\n" +
                        "\tResponse Json\n" +
                        "\t\tstatus\": \"1 or 0, 1=success; 0=error\",\n" +
                        "\t\tmessage\": \"xxx\",\n" +
                        "\t\tdata\": \"object\"\n" +
                        "\n")
                .contact(new Contact("longvan", "https://longvan.net", "support@longvan.net"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry
                .addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry
                .addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
