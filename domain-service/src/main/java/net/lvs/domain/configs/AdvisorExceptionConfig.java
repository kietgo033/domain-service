package net.lvs.domain.configs;

import net.lvs.domain.constants.MessageConstant;
import net.lvs.domain.dto.ResponseCustomBodyDTO;
import net.lvs.domain.exception.AuthenException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


@RestControllerAdvice
public class AdvisorExceptionConfig extends ResponseEntityExceptionHandler {
    public final Logger logger = Logger.getLogger(getClass().getName());
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity.badRequest().body(ResponseCustomBodyDTO.builder().data(errors).status(0).build());
    }

    @ExceptionHandler(AuthenException.class)
    public ResponseEntity<Object> handAuthenException(
            AuthenException ex, WebRequest request) {

        return new ResponseEntity(ResponseCustomBodyDTO.builder().message(ex.getMessage()).status(0).build(), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(UnknownHostException.class)
    protected ResponseEntity<Object> handleUnknownHostException() {
        return new ResponseEntity(ResponseCustomBodyDTO.builder().message(MessageConstant.EX_HOST_UNKNOW).status(0).build(),HttpStatus.BAD_REQUEST);
    }



}
