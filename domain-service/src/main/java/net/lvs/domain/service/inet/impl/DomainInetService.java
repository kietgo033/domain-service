package net.lvs.domain.service.inet.impl;

import net.lvs.domain.dto.RenewDomainDTO;
import net.lvs.domain.model.domain.*;
import net.lvs.domain.service.BaseDomainInetAPI;
import net.lvs.domain.dto.inet.*;
import net.lvs.domain.service.inet.IDomainInetService;
import net.lvs.domain.utils.CommandInetUtils;


import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 14:06 22/04/2022
 * Admin
 */
@Service
public class DomainInetService extends BaseDomainInetAPI implements IDomainInetService {

    @Override
    public ResultDomainModel createDomainInet(InetCreateDomainDTO inetCreateDomainDTO) throws Exception {
        ResultDomainModel resultDomainModel = null;
        String result = resultINet(inetCreateDomainDTO, CommandInetUtils.CREATE_DOMAIN);
        resultDomainModel = resultDomain(result);
        return resultDomainModel;
    }

    @Override
    public ResultDomainModel renewDomainInet(RenewDomainDTO renewDomainDTO) throws Exception {
        ResultDomainModel resultDomainModel = null;
        InetRenewDomainDTO inetRenewDomainDTO=new InetRenewDomainDTO(renewDomainDTO.getId(),renewDomainDTO.getPeriod());
        String result = resultINet(inetRenewDomainDTO, CommandInetUtils.RENEW_DOMAIN);
        resultDomainModel = resultDomain(result);
        return resultDomainModel;
    }

    @Override
    public List<ResultInfoDomainINetModel> searchDomainInet(DomainINetModel domainINetModel) throws Exception {
        try {
            String result = resultINet(domainINetModel, CommandInetUtils.INFO_DOMAIN);
            if (result != null) {
                JSONObject object = new JSONObject(result);
                List<ResultInfoDomainINetModel> list=resultInfo(object.get("content").toString());
                if (list!=null&&list.size()>0)
                    return list;
            }
            throw new Exception("Không tìm thấy domain phù hợp hoặc do cú pháp tìm kiếm không hợp lệ");
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    //tra ve true thi domain do da ton tai
    public boolean checkExistDomainInet(String domain) throws Exception {
        InetCheckExistDomainDTO inetCheckExistDomainDTO=new InetCheckExistDomainDTO(domain);
        String result = resultINet(inetCheckExistDomainDTO, CommandInetUtils.CHECK_DOMAIN);
        ResultDomainModel resultDomainModel = resultDomain(result);
        if (resultDomainModel != null) {
            if (resultDomainModel.getStatus().equals(ResultDomainModel.CHECK_OK)) {
                return false;
            }
            else if (resultDomainModel.getStatus().equals(ResultDomainModel.CHECK_FAIL)) {
                return true;
            }
            else if (resultDomainModel.getStatus().equals(ResultDomainModel.ERROR)) {
                throw new Exception(resultDomainModel.getMessage());
            }
        }
        throw new Exception("Check error");
    }

    @Override
    public ResultDomainModel getInfoDomainInet(String id) throws Exception {

        InetIdDTO inetIdDTO = new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.GET_INFO_DOMAIN);
        ResultDomainModel rs = resultDomain(result);

        if (rs.getStatus().equals(ResultDomainModel.ERROR))
            throw new Exception(rs.getMessage());
        else
            return rs;
    }

    @Override
    public boolean updateDNSDomainInet(InetUpdateDNSDomainDTO inetUpdateDNSDomainDTO) throws Exception {

        //DomainINetModel domainINetModel = mapper.map(inetUpdateDNSDomainDTO, DomainINetModel.class);
        String result = resultINet(inetUpdateDNSDomainDTO, CommandInetUtils.UPDATE_DNS);
        ResultDomainModel resultDomainModel = resultDomain(result);
        if (resultDomainModel != null) {
            if (!resultDomainModel.getStatus().equals(ResultDomainModel.ERROR))
                return true;
        }
        throw new Exception(resultDomainModel.getMessage());
    }

    @Override
    public boolean updateDNSChildDomainInet(InetUpdateDNSChildDTO inetUpdateDNSChildDTO) throws Exception {

        String result = resultINet(inetUpdateDNSChildDTO, CommandInetUtils.UPDATE_DNS_CHILD);
        ResultDomainModel resultDomainModel = resultDomain(result);
        if (resultDomainModel != null) {
            if (!resultDomainModel.getStatus().equals(ResultDomainModel.ERROR))
                return true;
        }
        throw new Exception(resultDomainModel.getMessage());
    }

    @Override
    public boolean privacyProtectionDomainInet(String id) throws Exception {
        InetIdDTO inetIdDTO = new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.PRIVACY_PROTECTION);
        ResultDomainModel rs = resultDomain(result);

        if (rs.getStatus().equals(ResultDomainModel.ERROR))
            throw new Exception(rs.getMessage());
        else
            return true;
    }

    @Override
    public ResultWhoIsModel checkWhoIsDomainInet(String domainName) throws Exception {

        InetDomainNameDTO inetDomainNameDTO=new InetDomainNameDTO(domainName);
        String result = resultINet(inetDomainNameDTO, CommandInetUtils.CHECK_WHOIS);
        ResultWhoIsModel resultWhoIsModel= resultWhoIs(result);
        if(resultWhoIsModel.getCode().equals("1"))
            throw new Exception(resultWhoIsModel.getMessage());
        return resultWhoIsModel;
    }

    @Override
    public boolean changePasswordDomainInet(InetChangePasswordDTO inetChangePasswordDTO) throws Exception {

        String result = resultINet(inetChangePasswordDTO, CommandInetUtils.CHANGE_PASSWORD_DOMAIN);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }


    /////////////////Chỉ áp dụng cho tên miền sử dụng DNS của iNET//////////////
    @Override
    public boolean enableDNSSEC(String id) throws Exception {
        InetIdDTO inetIdDTO=new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.ENABLE_DNSSEC);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public boolean syncDNSSEC(String id) throws Exception {
        InetIdDTO inetIdDTO=new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.SYNC_DNSSEC);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public boolean disableDNSSEC(String id) throws Exception {
        InetIdDTO inetIdDTO=new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.DISABLE_DNSSEC);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    //////////////Ap dụng cho tên miền không sử dụng DNS của iNET////////
    @Override
    public boolean createDNSSECManual(InetDNSSECManualDTO inetDnssecManualDTO) throws Exception {
        String result = resultINet(inetDnssecManualDTO, CommandInetUtils.CREATE_DNSSEC_MANUAL);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public boolean updateDNSSECManual(InetDNSSECManualDTO inetDnssecManualDTO) throws Exception {
        String result = resultINet(inetDnssecManualDTO, CommandInetUtils.UPDATE_DNSSEC_MANUAL);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public boolean deleteDNSSECManual(String id) throws Exception {
        InetIdDTO inetIdDTO=new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.DELETE_DNSSEC_MANUAL);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public ResultDomainModel getRecordDomainInet(String id) throws Exception {
        InetIdDTO inetIdDTO = new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.GET_RECORD_DOMAIN);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.ERROR))
            throw new Exception(rs.getMessage());
        else
            return rs;
    }


    // chưa rõ trả về gì... Chưa test được.
    @Override
    public boolean updateRecordDomainInet(InetUpdateRecordDomainDTO inetUpdateRecordDomainDTO) throws Exception {
        String result = resultINet(inetUpdateRecordDomainDTO, CommandInetUtils.UPDATE_RECORD_DOMAIN);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public boolean resendEmailVerificationDomainInet(String id) throws Exception {
        InetIdDTO inetIdDTO=new InetIdDTO(id);
        String result = resultINet(inetIdDTO, CommandInetUtils.RESEND_EMAIL_VERIFICATION_DOMAIN);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public boolean changeAuthcodeDomainInet(InetChangeAuthCodeDTO inetChangeAuthCodeDTO) throws Exception {

        String result = resultINet(inetChangeAuthCodeDTO, CommandInetUtils.CHANGE_AUTHCODE_DOMAIN);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }




    @Override
    public ResultDomainModel searchLogDomainInet(InetSearchLogDomainDTO inetSearchLogDomainDTO) throws Exception {
        return null;
    }

    @Override
    public boolean uploadContractDomainInet(InetUploadContractDTO inetUploadContractDTO) throws Exception {
        String result = resultINet(inetUploadContractDTO, CommandInetUtils.UPLOAD_CONTRACT);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }

    @Override
    public boolean uploadIdNumberDomainInet(InetUploadIdNumber inetUploadIdNumber) throws Exception {
        String result = resultINet(inetUploadIdNumber, CommandInetUtils.UPLOAD_ID_NUMBER);
        ResultDomainModel rs = resultDomain(result);
        if (rs.getStatus().equals(ResultDomainModel.SUCCESS))
            return true;
        else
            throw new Exception(rs.getMessage());
    }




}