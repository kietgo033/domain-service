package net.lvs.domain.service.pa;

import net.lvs.domain.dto.RenewDomainDTO;
import net.lvs.domain.dto.inet.InetChangePasswordDTO;
import net.lvs.domain.dto.inet.InetUpdateDNSChildDTO;
import net.lvs.domain.dto.inet.InetUpdateDNSDomainDTO;
import net.lvs.domain.model.domain.DomainPaModel;
import net.lvs.domain.model.domain.ResultDomainModel;
import net.lvs.domain.model.domain.ResultPaModel;

public interface IDomainPaService {
    ResultPaModel accountStill() throws Exception;

    ResultPaModel accountTotal() throws Exception;

    ResultPaModel getDateDomainPa(String domain) throws Exception;

    boolean checkExistDomainPa(String domain) throws Exception;

    boolean changePasswordDomainPa(String domain, String password) throws Exception;

    boolean updateDNSInternationalDomainPa(InetUpdateDNSDomainDTO inetUpdateDNSDomainDTO) throws Exception;

    boolean updateDNS_VN_DomainPa(InetUpdateDNSChildDTO inetUpdateDNSChildDTO) throws Exception;

    ResultDomainModel getInfoDomainPa (String domain) throws  Exception;

    int checkDomainPa (String domain) throws  Exception;

    ResultDomainModel createDomainPa (DomainPaModel domainPaModel) throws  Exception;
    ResultDomainModel renewDomainPa (RenewDomainDTO renewDomainDTO) throws  Exception;
    boolean updateDomainPa (DomainPaModel domainPaModel) throws  Exception;
}
