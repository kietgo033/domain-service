package net.lvs.domain.service;

import com.google.common.net.HttpHeaders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.lvs.domain.model.domain.DomainPaModel;
import net.lvs.domain.model.domain.ResultDomainModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * 27/04/2022 12:10
 * Phan Hoàng Kiệt
 */

public class BaseDomainPaAPI {
    private Logger logger = Logger.getLogger(getClass().getName());

    protected String tokenDomainPa = "206aa95153d4ed4f5fd401197bb97190";
    protected String urlDomainPa = "https://daily.pavietnam.vn/interface_test.php?";
    protected String username = "longvannet";

    public String returnResult(DomainPaModel domainPaModel, String cmd) {
        try {
            Gson gson = new Gson();
            if (domainPaModel != null) {
                domainPaModel.setUsername(username);
                domainPaModel.setApikey(tokenDomainPa);
                domainPaModel.setCmd(cmd);
                domainPaModel.setResponsetype("json");
                String strJsonBody = gson.toJson(domainPaModel);
                strJsonBody = strJsonBody.replace(":", "=");
                strJsonBody = strJsonBody.replace("\"", "");
                strJsonBody = strJsonBody.replace("{", "");
                strJsonBody = strJsonBody.replace("}", "");
                strJsonBody = strJsonBody.replace(",", "&");
                String urlTest = urlDomainPa;
                //String urlStr = uriEncode(urlTest + strJsonBody);
                String urlStr = urlTest + strJsonBody;
                URL url = new URL(urlStr);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, String.valueOf(MediaType.APPLICATION_JSON));
                OutputStream outputStream = conn.getOutputStream();
                outputStream.write(strJsonBody.getBytes());
                outputStream.flush();
                int httpResponse = conn.getResponseCode();
                if (httpResponse == 200) {
                    String result = readInputStreamToString(conn);
                    if (result != null) {
                        return result;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public HashMap<String, String> returnHashMap(String result) {
        HashMap<String, String> hashMap = new HashMap<>();
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        hashMap = gson.fromJson(result, type);
        if (null == hashMap) {
            hashMap = new HashMap<>();
        }
        return hashMap;
    }

    public ResultDomainModel returnInfoDomainPa(String result) {
        ResultDomainModel resultDomainModel = new ResultDomainModel();
        Gson gson = new Gson();
        Type type = new TypeToken<ResultDomainModel>() {
        }.getType();
        resultDomainModel = gson.fromJson(result, type);
        return resultDomainModel;
    }

    public Object returnObject(String result,Type type) {
        Gson gson = new Gson();
        Object object= gson.fromJson(result, type);
        return object;
    }

    public boolean checkDomain(String result) {
        if (result.equals("1"))
            return true;
        else {
            return false;
        }
    }

    public static String readInputStreamToString(HttpURLConnection connection) {
        String result = null;
        StringBuilder sb = new StringBuilder();
        InputStream is = null;

        try {
            is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        } catch (Exception e) {
            result = null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }
        return result;
    }
    private static String uriEncode(String string) {
        String result = string;
        if (null != string) {
            try {
                String scheme = null;
                String ssp = string;
                int es = string.indexOf(':');
                if (es > 0) {
                    scheme = string.substring(0, es);
                    ssp = string.substring(es + 1);
                }
                result = (new URI(scheme, ssp, null)).toString();
            } catch (URISyntaxException usex) {
            }
        }
        return result;
    }



}
