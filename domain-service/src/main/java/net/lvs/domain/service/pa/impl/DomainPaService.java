package net.lvs.domain.service.pa.impl;

import com.google.gson.reflect.TypeToken;
import net.lvs.domain.dto.RenewDomainDTO;
import net.lvs.domain.dto.inet.InetUpdateDNSChildDTO;
import net.lvs.domain.dto.inet.InetUpdateDNSDomainDTO;
import net.lvs.domain.model.domain.DomainPaModel;
import net.lvs.domain.model.domain.ResultDomainModel;
import net.lvs.domain.model.domain.ResultFullPaModel;
import net.lvs.domain.model.domain.ResultPaModel;
import net.lvs.domain.service.BaseDomainPaAPI;
import net.lvs.domain.service.pa.IDomainPaService;
import net.lvs.domain.utils.CommandPaUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;

/**
 * 27/04/2022 14:12
 * Phan Hoàng Kiệt
 */
@Service
public class DomainPaService extends BaseDomainPaAPI implements IDomainPaService {

    @Override
    public ResultPaModel accountStill() throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        String result = returnResult(domainPaModel, CommandPaUtils.CHECK_ACCOUNT_STILL_AGENCY);
        Type type = new TypeToken<ResultPaModel>() {
        }.getType();
        ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
        if (resultPaModel.getReturnText() != null)
            throw new Exception(resultPaModel.getReturnText());
        return resultPaModel;
    }

    @Override
    public ResultPaModel accountTotal() throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        String result = returnResult(domainPaModel, CommandPaUtils.CHECK_ACCOUNT_TOTAL_AGENCY);
        Type type = new TypeToken<ResultPaModel>() {
        }.getType();
        ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
        if (resultPaModel.getReturnText() != null)
            throw new Exception(resultPaModel.getReturnText());
        return resultPaModel;
    }

    @Override
    public ResultPaModel getDateDomainPa(String domain) throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        domainPaModel.setDomain(domain);
        String result = returnResult(domainPaModel, CommandPaUtils.GET_DATE_DOMAIN);
        Type type = new TypeToken<ResultPaModel>() {
        }.getType();
        ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
        if (resultPaModel.getReturnText() != null)
            if (resultPaModel.getReturnCode() != 0)
                throw new Exception(resultPaModel.getReturnText());
        return resultPaModel;
    }

    @Override
    // tra ve true khi ten mien da duoc dang ky, false khi ten mien chua duoc dang ky
    public boolean checkExistDomainPa(String domain) throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        domainPaModel.setDomain(domain);
        String result = returnResult(domainPaModel, CommandPaUtils.CHECK_WHOIS);
        if (result.equals("1"))
            return false;
        else if (result.equals("0"))
            return true;
        else
            throw new Exception("Tên miền không hợp lệ");

    }

    @Override
    public boolean changePasswordDomainPa(String domain, String password) throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        domainPaModel.setDomain(domain);
        domainPaModel.setPasswordDomain(password);
        String result = returnResult(domainPaModel, CommandPaUtils.CHANGE_PASSWORD_DOMAIN);
        Type type = new TypeToken<ResultPaModel>() {
        }.getType();
        ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
        if (resultPaModel != null) {
            if (resultPaModel.getReturnCode() == 200)
                return true;
            throw new Exception(resultPaModel.getReturnText());
        }
        throw new Exception("Đổi password thất bại");
    }

    @Override
    public boolean updateDNSInternationalDomainPa(InetUpdateDNSDomainDTO inetUpdateDNSDomainDTO) throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        domainPaModel.setDomain(inetUpdateDNSDomainDTO.getId());
        domainPaModel.setDomainDNS1(inetUpdateDNSDomainDTO.getNsList().get(0).getHostName());
        domainPaModel.setDomainDNS2(inetUpdateDNSDomainDTO.getNsList().get(1).getHostName());
        if (inetUpdateDNSDomainDTO.getNsList().size() == 3)
            domainPaModel.setDomainDNS3(inetUpdateDNSDomainDTO.getNsList().get(2).getHostName());
        else if (inetUpdateDNSDomainDTO.getNsList().size() == 4) {
            domainPaModel.setDomainDNS3(inetUpdateDNSDomainDTO.getNsList().get(2).getHostName());
            domainPaModel.setDomainDNS4(inetUpdateDNSDomainDTO.getNsList().get(3).getHostName());
        }

        String result = returnResult(domainPaModel, CommandPaUtils.CHANGE_DNS_DOMAIN_INTERNATIONAL);
        Type type = new TypeToken<ResultPaModel>() {
        }.getType();
        ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
        if (resultPaModel != null) {
            if (resultPaModel.getReturnCode() == 200)
                return true;
            throw new Exception(resultPaModel.getReturnText());
        }
        throw new Exception("Thay đổi DNS cho domain quốc tế thất bại");
    }

    @Override
    public boolean updateDNS_VN_DomainPa(InetUpdateDNSChildDTO inetUpdateDNSChildDTO) throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        domainPaModel.setDomain(inetUpdateDNSChildDTO.getId());

        domainPaModel.setDomainDNS1(inetUpdateDNSChildDTO.getHostList().get(0).getHostName());
        domainPaModel.setDomainIP1(inetUpdateDNSChildDTO.getHostList().get(0).getIpv4());

        domainPaModel.setDomainDNS2(inetUpdateDNSChildDTO.getHostList().get(1).getHostName());
        domainPaModel.setDomainIP2(inetUpdateDNSChildDTO.getHostList().get(1).getIpv4());

        if (inetUpdateDNSChildDTO.getHostList().size() > 2) {
            domainPaModel.setDomainDNS3(inetUpdateDNSChildDTO.getHostList().get(2).getHostName());
            domainPaModel.setDomainIP3(inetUpdateDNSChildDTO.getHostList().get(2).getIpv4());
        }

        String result = returnResult(domainPaModel, CommandPaUtils.CHANGE_DNS_DOMAIN_VN);
        Type type = new TypeToken<ResultPaModel>() {
        }.getType();
        ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
        if (resultPaModel != null) {
            if (resultPaModel.getReturnCode() == 200)
                return true;
            throw new Exception(resultPaModel.getReturnText());
        }
        throw new Exception("Thay đổi DNS cho domain Việt Nam thất bại");
    }

    @Override
    public ResultDomainModel getInfoDomainPa(String domain) throws Exception {
        DomainPaModel domainPaModel = new DomainPaModel();
        domainPaModel.setDomain(domain);
        String result = returnResult(domainPaModel, CommandPaUtils.GET_INFO_DOMAIN);
        try {
            ResultDomainModel resultDomainModel = returnInfoDomainPa(result);
            if (resultDomainModel != null) {
                if (resultDomainModel.getReturnCode() == 200)
                    return resultDomainModel;
            }
        } catch (Exception e) {
            Type type = new TypeToken<ResultPaModel>() {
            }.getType();
            ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
            throw new Exception(resultPaModel.getReturnText());
        }
        throw new Exception("Có lỗi trong quá trình tìm kiếm thông tin tên miền " + domain);
    }

    @Override
    public int checkDomainPa(String domain) throws Exception {
        try {
            DomainPaModel domainPaModel = new DomainPaModel();
            domainPaModel.setDomain(domain);
            String result = returnResult(domainPaModel, CommandPaUtils.CHECK_DOMAIN_OF_AGENCY);
            Type type = new TypeToken<ResultPaModel>() {
            }.getType();
            ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
            if (resultPaModel != null) {
                return resultPaModel.getReturnCode();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        throw new Exception("Error");
    }

    @Override
    public ResultDomainModel createDomainPa(DomainPaModel domainPaModel) throws Exception {
        try {
            String result;
            domainPaModel.setReturn_full("1");
            if (domainPaModel.getDomainExt().toLowerCase().contains("vn"))
                result = returnResult(domainPaModel, CommandPaUtils.CREATE_DOMAIN_VN);
            else
                result = returnResult(domainPaModel, CommandPaUtils.CREATE_DOMAIN_INTERNATIONAL);

            Type type = new TypeToken<ResultFullPaModel>() {
            }.getType();
            ResultFullPaModel resultFullPaModel = (ResultFullPaModel) returnObject(result, type);
            if (resultFullPaModel != null) {
                if (resultFullPaModel.getReturnCode()==200)
                {
                    //tạo mới ResultDomainModel và trả về nó
                    ResultDomainModel resultDomainModel=new ResultDomainModel();
                    resultDomainModel.setResultFullPaModel(resultFullPaModel);
                    return resultDomainModel;
                }
                throw new Exception(resultFullPaModel.getReturnText());
            }
            throw new Exception("Result null");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public ResultDomainModel renewDomainPa(RenewDomainDTO renewDomainDTO) throws Exception {
        try {
            String result;
            DomainPaModel domainPaModel = new DomainPaModel();
            domainPaModel.setDomain(renewDomainDTO.getDomain());
            domainPaModel.setYear(renewDomainDTO.getYear());
            domainPaModel.setSendmail(renewDomainDTO.getSentMail());
            domainPaModel.setReturn_full("1");
            result = returnResult(domainPaModel, CommandPaUtils.RENEW_DOMAIN);

            Type type = new TypeToken<ResultFullPaModel>() {
            }.getType();
            ResultFullPaModel resultFullPaModel = (ResultFullPaModel) returnObject(result, type);
            if (resultFullPaModel != null) {
                if (resultFullPaModel.getReturnCode()==200) {
                    //tạo mới ResultDomainModel và trả về nó
                    ResultDomainModel resultDomainModel=new ResultDomainModel();
                    resultDomainModel.setResultFullPaModel(resultFullPaModel);
                    return resultDomainModel;
                }
                throw new Exception(resultFullPaModel.getReturnText());
            }
            throw new Exception("Result null");
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public boolean updateDomainPa(DomainPaModel domainPaModel) throws Exception {
        try {
            String result;
            result = returnResult(domainPaModel, CommandPaUtils.UPDATE_INFO_DOMAIN);
            Type type = new TypeToken<ResultPaModel>() {
            }.getType();
            ResultPaModel resultPaModel = (ResultPaModel) returnObject(result, type);
            if (resultPaModel != null) {
                if (resultPaModel.getReturnCode()==200) {
                    return true;
                }
                throw new Exception(resultPaModel.getReturnText());
            }
            throw new Exception("Result null");
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }





}
