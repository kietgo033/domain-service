package net.lvs.domain;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@EnableRetry
public class DomainApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(DomainApplication.class, args);
	}
	@Bean
	ModelMapper getModelMapper(){
		return  new ModelMapper();
	}

}
