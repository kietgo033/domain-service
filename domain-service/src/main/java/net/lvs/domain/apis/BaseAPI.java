package net.lvs.domain.apis;

import com.mongodb.MongoSocketOpenException;
import lombok.extern.slf4j.Slf4j;
import net.lvs.domain.constants.MessageConstant;
import net.lvs.domain.dto.ResponseCustomBodyDTO;
import net.lvs.domain.service.inet.*;
import net.lvs.domain.service.pa.IDomainPaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;

@Slf4j
@Retryable(
        value = {
                MongoSocketOpenException.class,
                RuntimeException.class,
                StringIndexOutOfBoundsException.class},
        maxAttemptsExpression = "${retry.maxAttempts}",
        backoff = @Backoff(delayExpression = "${retry.maxDelay}")
)
public class BaseAPI {
    @Recover
    public ResponseEntity Recovery(MongoSocketOpenException cone,
                                   RuntimeException rune,
                                   StringIndexOutOfBoundsException stre) {

        log.error("---retry false---");
        return ResponseEntity.internalServerError().body(ResponseCustomBodyDTO.builder().status(0).message(MessageConstant.TRY_AFTER).build());
    }

    @Autowired
    protected IDomainInetService domainInetService;
    @Autowired
    protected ICustomerDomainInetService customerDomainInetService;
    @Autowired
    protected IDomainPaService domainPaService;
}
