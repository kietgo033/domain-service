package net.lvs.domain.apis.domain;

import io.swagger.v3.oas.annotations.Operation;
import net.lvs.domain.apis.BaseAPI;
import net.lvs.domain.dto.CreateDomainDTO;
import net.lvs.domain.dto.RenewDomainDTO;
import net.lvs.domain.dto.inet.*;
import net.lvs.domain.handler.DomainServiceException;
import net.lvs.domain.model.domain.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/domain")
public class DomainAPI extends BaseAPI {


    @PostMapping("/inet/search")
    @Operation(summary = "Tiem kiem domain Inet", tags = "Domain Inet")
    public List<ResultInfoDomainINetModel> searchInfoDomainINet(
            @RequestBody DomainINetModel domainINetModel) {
        List<ResultInfoDomainINetModel> rs = null;
        try {
            rs = domainInetService.searchDomainInet(domainINetModel);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    // Trả về true thì có nghĩa là domain đó đã tồn tại
    @GetMapping("/exist")
    @Operation(summary = "Kiểm tra sự tồn tại của domain", tags = "Domain Chung")
    public Boolean checkExistDomainINet(
            @RequestParam (value = "domain") String domain,
            @RequestParam(value = "configKey") String configKey) {
        try {
            boolean rs=true;
            if (configKey.toLowerCase().contains("inet"))
                rs = domainInetService.checkExistDomainInet(domain);
            else if (configKey.toLowerCase().contains("pa"))
                rs=domainPaService.checkExistDomainPa(domain);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @GetMapping("/details/{id}")
    @Operation(summary = "Lấy thông tin domain của Inet(id) hoặc Pa(domain)", tags = "Domain Chung")
    public ResultDomainModel getInfoDomain(
            @PathVariable(name = "id") String id,
            @RequestParam(value = "configKey") String configKey) {
        ResultDomainModel resultDomainModel=null;
        try {
            if (configKey.toLowerCase().contains("inet")){
                resultDomainModel=domainInetService.getInfoDomainInet(id);
                return resultDomainModel;
            }
            else if (configKey.toLowerCase().contains("pa")){
                resultDomainModel=domainPaService.getInfoDomainPa(id);
                return resultDomainModel;
            }
            throw new DomainServiceException("Error configKey");
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PutMapping("/dns")
    @Operation(summary = "Cập nhật DNS cho domain Quốc tế của Pa hoặc thay đổi DNS của Inet", tags = "Domain Chung")
    public Boolean updateDNSDomain(
            @RequestBody InetUpdateDNSDomainDTO inetUpdateDNSDomainDTO,
            @RequestParam(value = "configKey") String configKey)
             {
        try {
            boolean rs=false;
            if (configKey.toLowerCase().contains("inet"))
                rs = domainInetService.updateDNSDomainInet(inetUpdateDNSDomainDTO);
            else if (configKey.toLowerCase().contains("pa"))
                rs=domainPaService.updateDNSInternationalDomainPa(inetUpdateDNSDomainDTO);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
//    @PostMapping("/update-dns-child")
//    @Operation(summary = "Cập nhật nameserver theo tên miền(childdns) của Inet hoặc cập nhật dns cho domain Viet nam của Pa", tags = "Domain Chung")
//    public Boolean updateDNSChildDomain(
//            @RequestBody InetUpdateDNSChildDTO inetUpdateDNSChildDTO,
//            @RequestParam(value = "configKey") String configKey) {
//        try {
//            boolean rs=false;
//            if (configKey.toLowerCase().contains("inet"))
//                rs = domainInetService.updateDNSChildDomainInet(inetUpdateDNSChildDTO);
//            else if (configKey.toLowerCase().contains("pa"))
//                rs=domainPaService.updateDNS_VN_PaDomain(inetUpdateDNSChildDTO);
//            return rs;
//        } catch (Exception e) {
//            throw new DomainServiceException(e.getMessage());
//        }
//    }

    @PutMapping("/dns-child")
    @Operation(summary = "Cập nhật DNS child", tags = "Domain Chung")
    public Boolean updateDNSDomain(
            @RequestBody InetUpdateDNSChildDTO inetUpdateDNSChildDTO,
            @RequestParam(value = "configKey") String configKey) {
        boolean rs = false;
        try {
            if (configKey.toLowerCase().contains("inet"))
                rs = domainInetService.updateDNSChildDomainInet(inetUpdateDNSChildDTO);
            else if (configKey.toLowerCase().contains("pa"))
                rs = domainPaService.updateDNS_VN_DomainPa(inetUpdateDNSChildDTO);
            return rs;
        }catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PostMapping("/renew")
    @Operation(summary = "Gia hạn domain", tags = "Domain Chung")
    public ResultDomainModel renewDomainINet(
            @RequestBody RenewDomainDTO renewDomainDTO,
            @RequestParam(value = "configKey") String configKey) {
        try {
            ResultDomainModel rs=null;
            if (configKey.toLowerCase().contains("inet"))
                rs = domainInetService.renewDomainInet(renewDomainDTO);
            else if (configKey.toLowerCase().contains("pa"))
                rs = domainPaService.renewDomainPa(renewDomainDTO);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @GetMapping("/privacy-protection/{id}")
    @Operation(summary = "Ẩn thông tin domain", tags = "Domain Inet")
    public Boolean privacyProtectionDomainINet(
            @PathVariable(name = "id") String id) {
        try {
            boolean rs = domainInetService.privacyProtectionDomainInet(id);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
    @GetMapping("/whois/{domainName}")
    @Operation(summary = "Check thông tin whois domain", tags = "Domain Inet")
    public ResultWhoIsModel checkWhoIsDomainINet(
            @PathVariable(name = "domainName") String domainName) {
        try {
            ResultWhoIsModel rs = domainInetService.checkWhoIsDomainInet(domainName);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PostMapping("/customer")
    @Operation(summary = "Tạo tài khoản khách hàng khi tk LV chưa có tài khoản của bên INet", tags = "Customer Inet")
    public ContactINetModel createAccountCustomerINet(
            @RequestBody ContactINetModel contactINetModel) {
        try {
            ContactINetModel rs = customerDomainInetService.creatAccount(contactINetModel);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
    @GetMapping("/customer/suspend/{id}")
    @Operation(summary = "Tam ngưng tài khoản", tags = "Customer Inet")
    public Boolean suspendAccountCustomerINet(
            @PathVariable(name = "id") String id) {
        try {
            boolean rs = customerDomainInetService.suspendAccount(id);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @GetMapping("/customer/active/{id}")
    @Operation(summary = "Kích hoạt tài khoản", tags = "Customer Inet")
    public Boolean activeAccountCustomerINet(
            @PathVariable(name = "id") String id) {
        try {
            boolean rs = customerDomainInetService.activeAccount(id);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @GetMapping("/customer/getbyid/{id}")
    @Operation(summary = "Lấy thông tin tài khoản theo id account", tags = "Customer Inet")
    public ContactINetModel getAccountCustomerINetById(
            @PathVariable(name = "id") String id) {
        try {
            ContactINetModel rs = customerDomainInetService.getAccountById(id);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @GetMapping("/customer/getbyemail/{email}")
    @Operation(summary = "Lấy thông tin tài khoản theo email account", tags = "Customer Inet")
    public ContactINetModel getAccountCustomerINetByEmail(
            @PathVariable(name = "email") String email) {
        try {
            ContactINetModel rs = customerDomainInetService.getAccountByEmail(email);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }






    //------------------Pa API-----------------------

    @GetMapping("/pa/account-still")
    @Operation(summary = "Kiểm tra số tiền con lại của đại lý", tags = "Domain Pa")
    public ResultPaModel getAccountStill(){
        try {
            ResultPaModel rs = domainPaService.accountStill();
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @GetMapping("/pa/account-total")
    @Operation(summary = "Kiểm tra tổng số tiền đã nạp của đại lý", tags = "Domain Pa")
    public ResultPaModel getAccountTotal(){
        try {
            ResultPaModel rs = domainPaService.accountTotal();
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @GetMapping("/pa/date")
    @Operation(summary = "Lấy thông tin ngày đăng ký và ngày hết hạn của domain", tags = "Domain Pa")
    public ResultPaModel getDateDomainPa(@RequestParam(value = "domain") String domain){
        try {
            ResultPaModel rs = domainPaService.getDateDomainPa(domain);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
    @GetMapping("/pa/check-domain")
    @Operation(summary = "Kiểm tra domain có thuộc quyền quản lý của PA hay không(1: Thuộc, 2: Hết hạn, 3:Không thuộc)", tags = "Domain Pa")
    public Integer checkDomainPa(@RequestParam(value = "domain") String domain){
        try {
            int rs = domainPaService.checkDomainPa(domain);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PutMapping("/password")
    @Operation(summary = "Thay đổi password của domain", tags = "Domain Chung")
    public Boolean changePasswordDomain(@RequestBody InetChangePasswordDTO inetChangePasswordDTO,
                                        @RequestParam(value = "configKey") String configKey){
        try {
            boolean rs=false;
            if (configKey.toLowerCase().contains("inet"))
                rs = domainInetService.changePasswordDomainInet(inetChangePasswordDTO);
            else if (configKey.toLowerCase().contains("pa"))
                rs=domainPaService.changePasswordDomainPa(inetChangePasswordDTO.getId(),inetChangePasswordDTO.getPassword());
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PostMapping("")
    @Operation(summary = "Tạo mới domain Pa hoặc Inet", tags = "Domain Chung")
    public ResultDomainModel createDomain(@RequestBody CreateDomainDTO createDomainDTO,
                                          @RequestParam(value = "configKey") String configKey){
        try {
            ResultDomainModel rs=null;
            if (configKey.toLowerCase().contains("inet"))
                rs = domainInetService.createDomainInet(createDomainDTO.getInetCreateDomainDTO());
            else if (configKey.toLowerCase().contains("pa"))
                rs=domainPaService.createDomainPa(createDomainDTO.getDomainPaModel());
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PutMapping("/pa")
    @Operation(summary = "Cập nhật thông tin cho domain Pa (Chỉ áp dụng cho các domain quốc tế)", tags = "Domain Pa")
    public Boolean updateDomainPa(@RequestBody DomainPaModel domainPaModel){
        try {
            boolean rs=false;
            rs=domainPaService.updateDomainPa(domainPaModel);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }



    ///////////// DNSSEC domain---------Chỉ áp dụng cho tên miền sử dụng DNS của iNET///////////////

    @PostMapping("/inet/enable-dnssec")
    @Operation(summary = "Kys DNSSEC", tags = "Domain Inet")
    public Boolean enableDNSSEC(@RequestBody InetIdDTO inetIdDTO){
        try {
            boolean rs=false;
            rs = domainInetService.enableDNSSEC(inetIdDTO.getId());
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
    @PostMapping("/inet/sync-dnssec")
    @Operation(summary = "Đồng bộ bản ghi DNSSEC lên EPP server", tags = "Domain Inet")
    public Boolean syncDNSSEC(@RequestBody InetIdDTO inetIdDTO){
        try {
            boolean rs=false;
            rs = domainInetService.syncDNSSEC(inetIdDTO.getId());
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
    @PostMapping("/inet/disable-dnssec")
    @Operation(summary = "Bỏ ký DNSSEC cho tên miền", tags = "Domain Inet")
    public Boolean disableDNSSEC(@RequestBody InetIdDTO inetIdDTO){
        try {
            boolean rs=false;
            rs = domainInetService.disableDNSSEC(inetIdDTO.getId());
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }


    ////////////////////Ap dụng cho tên miền không sử dụng DNS của iNET////////////////

    @PostMapping("/inet/create-dnssec-manual")
    @Operation(summary = "Ký DNSSEC thủ công")
    public Boolean createdDNSSECManual(@RequestBody InetDNSSECManualDTO inetDnssecManualDTO){
        try {
            boolean rs=false;
            rs = domainInetService.createDNSSECManual(inetDnssecManualDTO);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
    @PostMapping("/inet/update-dnssec-manual")
    @Operation(summary = "Cập nhật khóa bản ghi DNSSEC trước khi đồng bộ")
    public Boolean updateDNSSECManual(@RequestBody InetDNSSECManualDTO inetDnssecManualDTO){
        try {
            boolean rs=false;
            rs = domainInetService.updateDNSSECManual(inetDnssecManualDTO);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }
    @PostMapping("/inet/delete-dnssec-manual")
    @Operation(summary = "Cập nhật khóa bản ghi DNSSEC trước khi đồng bộ")
    public Boolean updateDNSSECManual(@RequestBody InetIdDTO inetIdDTO){
        try {
            boolean rs=false;
            rs = domainInetService.deleteDNSSECManual(inetIdDTO.getId());
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    ////////-----------recordList Inet--------------////////////

    @GetMapping("/inet/records/{id}")
    @Operation(summary = "Lấy thông tin bản ghi tên miền")
    public ResultDomainModel getRecordDomainInet(@PathVariable(name = "id") String id ){
        try {
            ResultDomainModel rs = domainInetService.getRecordDomainInet(id);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PutMapping("/inet/records")
    @Operation(summary = "Cập nhật thông tin bản ghi tên miền")
    public Boolean updateRecordDomainInet(@RequestBody InetUpdateRecordDomainDTO inetUpdateRecordDomainDTO){
        try {
            boolean rs = domainInetService.updateRecordDomainInet(inetUpdateRecordDomainDTO);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    /////-------------resendemailverification, changeauthcode-----------////////

    @PostMapping("/inet/resend-email")
    @Operation(summary = "Gửi lại email để xác nhận tên miền Inet")
    public Boolean resendEmailDomainInet(@RequestParam(value = "id") String id ){
        try {
            boolean rs = domainInetService.resendEmailVerificationDomainInet(id);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PutMapping("/inet/authcode")
    @Operation(summary = "Đổi lại mã auth code cho tên miền Inet")
    public Boolean changeAuthCodeDomainInet(@RequestBody InetChangeAuthCodeDTO inetChangeAuthCodeDTO ){
        try {
            boolean rs = domainInetService.changeAuthcodeDomainInet(inetChangeAuthCodeDTO);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    ////////-----upload inet------//////////

    @PostMapping("/inet/contract")
    @Operation(summary = "Upload bản khai tên miền thông qua url và id")
    public Boolean uploadContractDomainInet(@RequestBody InetUploadContractDTO inetUploadContractDTO ){
        try {
            boolean rs = domainInetService.uploadContractDomainInet(inetUploadContractDTO);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }

    @PostMapping("/inet/contact/id-number")
    @Operation(summary = "Uploa chứng minh thư của khách hàng")
    public Boolean uploadIdNumberDomainInet(@RequestBody InetUploadIdNumber inetUploadIdNumber ){
        try {
            boolean rs = domainInetService.uploadIdNumberDomainInet(inetUploadIdNumber);
            return rs;
        } catch (Exception e) {
            throw new DomainServiceException(e.getMessage());
        }
    }






}
